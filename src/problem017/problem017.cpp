#include "runner.hpp"
#include "problem017.hpp"

namespace problem017 {
    uint32_t ones_sizes[] = {
        0, // <zero>
        3, // one
        3, // two
        5, // three
        4, // four
        4, // five
        3, // six
        5, // seven
        5, // eight
        4, // nine
    };
    uint32_t teens_sizes[] = {
        3, // ten
        6, // eleven
        6, // twelve
        8, // thirteen
        8, // fourteen
        7, // fifteen
        7, // sixteen
        9, // seventeen
        8, // eighteen
        8, // nineteen
    };
    uint32_t tens_sizes[] = {
        0, // <zero>
        0, // <special - see teens_sizes>
        6, // twenty
        6, // thirty
        5, // forty
        5, // fifty
        5, // sixty
        7, // seventy
        6, // eighty
        6, // ninety
    };
    uint32_t number_size(uint32_t num) {
        if (num <= 9)
            return ones_sizes[num];
        if (num <= 19)
            return teens_sizes[num-10];
        if (num <= 99)
            return tens_sizes[num/10] + ones_sizes[num%10];
        if (num <= 999) {
            if (num % 100 == 0)
                return ones_sizes[num/100] + 7; // 7: hundred
            else
                return ones_sizes[num/100] + 7 + 3 + number_size(num%100); // 3: and
        }
        if (num == 1000)
            return 11; // 11: one thousand
        throw std::exception();
    }

    euler_result run() {
        uint32_t sum = 0;
        for (uint32_t x = 1; x <= 1000; x++) {
            sum += number_size(x);
        }
        return sum;
    }

    euler_entry entry(run);
}
