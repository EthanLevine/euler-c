#include <gmpxx.h>

#include "runner.hpp"
#include "problem063.hpp"

namespace problem063 {
    euler_result run() {
        // #digits (n) = floor(1 + log10(n))
        // so we want to find floor(1 + log10(x^y)) == y
        // we know 1 <= x <= 9, (with x=10, floor(1+log10(10^y)) = 1+y > y)
        uint32_t count = 0;
        for (uint32_t x = 1; x <= 9; x++) {
            for (uint32_t y = 1; ; y++) {
                mpz_class power;
                mpz_ui_pow_ui(power.get_mpz_t(), x, y);
                // simplest way to get digit count is to convert to string
                if (y == power.get_str().size())
                    count++;
                else
                    break;
            }
        }
        return count;
    }

    euler_entry entry(run);
}
