#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem058.hpp"

namespace problem058 {
    euler_result run() {
        uint64_t limit = 1000000000;
        std::vector<bool> sieve = build_prime_sieve(limit);
        uint64_t diag_primes = 0;
        const double target_ratio = 0.1;
        for (uint64_t x = 1; ; x++) {
            uint64_t start = 4*x*x - 2*x + 1;
            uint64_t step = 2*x;
            // note: the last diagonal is never prime
            for (uint64_t y = 0; y < 3; y++) {
                uint64_t diag = start + step*y;
                if (sieve[diag])
                    diag_primes++;
            }
            if ((double)diag_primes / (4*x+1) < target_ratio)
                return 2*x+1;
        }
        return 0;
    }

    euler_entry entry(run);
}
