#include <cmath>
#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem027.hpp"

namespace problem027 {
    euler_result run() {
        std::vector<bool> prime_sieve = build_prime_sieve((uint32_t) 1000);
        int32_t max_a = 0;
        int32_t max_b = 0;
        int32_t max_count = 0;
        for (int32_t a = -999; a <= 999; a++) {
            for (int32_t b = -999; b <= 999; b++) {
                int32_t n = -1;
                int32_t value;
                do {
                    n++;
                    value = n*n + a*n + b;
                } while (value > 1 && prime_sieve[value]);
                if (n > max_count) {
                    max_count = n;
                    max_a = a;
                    max_b = b;
                }
            }
        }
        return max_a * max_b;
    }

    euler_entry entry(run);
}
