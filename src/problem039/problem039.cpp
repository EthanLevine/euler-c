#include <cmath>

#include "runner.hpp"
#include "problem039.hpp"

namespace problem039 {
    uint32_t count_solutions(uint32_t p) {
        uint32_t count = 0;
        for (uint32_t a = 1; a < p/2; a++) {
            for (uint32_t b = a+1; b < p-a-1; b++) {
                uint32_t c_sq = a*a + b*b;
                uint32_t c = (uint32_t)sqrt(c_sq);
                if (c*c == c_sq && a+b+c == p)
                    count++;
            }
        }
        return count;
    }

    euler_result run() {
        uint32_t max_count = 0;
        uint32_t max_p = 0;
        for (uint32_t p = 5; p <= 1000; p++) {
            uint32_t count = count_solutions(p);
            if (count > max_count) {
                max_count = count;
                max_p = p;
            }
        }
        return max_p;
    }

    euler_entry entry(run);
}
