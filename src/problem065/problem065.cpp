#include <gmpxx.h>

#include "runner.hpp"
#include "problem065.hpp"

namespace problem065 {
    // index is 1-based, so this function returns <undef>, 1, 2, 1, 1, 4, 1, 1, 6, 1, etc.
    uint32_t get_repeated_denom(uint32_t index) {
        if (index % 3 == 2) {
            return 2 * (1 + index/3);
        } else {
            return 1;
        }
    }
    euler_result run() {
        const uint32_t convergent_number = 100;
        mpq_class convergent(0);
        for (uint32_t denom_index = convergent_number-1; denom_index > 0; denom_index--) {
            convergent = 1 / (convergent + get_repeated_denom(denom_index));
        }
        convergent += 2;
        convergent.canonicalize();
        mpz_class numerator = convergent.get_num();
        uint32_t digit_sum = 0;
        while (numerator > 0) {
            mpz_class digit = numerator % 10;
            digit_sum += digit.get_ui();
            numerator /= 10;
        }
        return digit_sum;
    }

    euler_entry entry(run);
}
