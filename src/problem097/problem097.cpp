#include <string>
#include <gmpxx.h>

#include "runner.hpp"
#include "problem097.hpp"

namespace problem097 {
    euler_result run() {
        mpz_class coeff = 28433_mpz;
        uint32_t expo = 7830457;
        mpz_class result;
        mpz_mul_2exp(result.get_mpz_t(), coeff.get_mpz_t(), expo);
        result += 1;
        std::string result_str = result.get_str();
        return result_str.substr(result_str.size() - 10);
    }

    euler_entry entry(run);
}
