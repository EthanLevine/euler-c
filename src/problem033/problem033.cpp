#include <cmath>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem033.hpp"

namespace problem033 {
    void simplify(uint32_t& num, uint32_t& denom) {
        uint32_t frac_gcd = gcd(num, denom);
        num /= frac_gcd;
        denom /= frac_gcd;
    }

    bool are_frac_equal(uint32_t num1, uint32_t denom1, uint32_t num2, uint32_t denom2) {
        simplify(num1, denom1);
        simplify(num2, denom2);
        return (num1 == num2) && (denom1 == denom2);
    }

    bool is_cancellable(uint32_t num, uint32_t denom) {
        // discard "trivial" examples
        if (num % 10 == 0 || denom % 10 == 0)
            return false;
        uint32_t num_a = num / 10;
        uint32_t num_b = num % 10;
        uint32_t denom_a = denom / 10;
        uint32_t denom_b = denom % 10;
        // try all 4 calcellations
        if (num_a == denom_a && num_b < denom_b) {
            if (are_frac_equal(num, denom, num_b, denom_b))
                return true;
        }
        if (num_a == denom_b && num_b < denom_a) {
            if (are_frac_equal(num, denom, num_b, denom_a))
                return true;
        }
        if (num_b == denom_a && num_a < denom_b) {
            if (are_frac_equal(num, denom, num_a, denom_b))
                return true;
        }
        if (num_b == denom_b && num_a < denom_a) {
            if (are_frac_equal(num, denom, num_a, denom_a))
                return true;
        }
        return false;
    }

    euler_result run() {
        uint32_t prod_num = 1;
        uint32_t prod_denom = 1;
        for (uint32_t num = 11; num <= 99; num++) {
            for (uint32_t denom = num + 1; denom <= 99; denom++) {
                if (is_cancellable(num, denom)) {
                    prod_num *= num;
                    prod_denom *= denom;
                }
            }
        }
        simplify(prod_num, prod_denom);
        return prod_denom;
    }

    euler_entry entry(run);
}
