#include "runner.hpp"
#include "problem019.hpp"

namespace problem019 {
    // note: 0=sunday, 6=saturday
    uint32_t count_first_sundays(uint32_t start_year, uint32_t start_day) {
        if (start_year == 2001)
            return 0;
        uint32_t count = 0;
        uint32_t month_days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (start_year % 4 == 0) {
            // leap year
            // note: the leap year rules are more complicated, but this fits for 1901-2000
            month_days[1] = 29;
        }
        for (uint32_t x = 0; x < 12; x++) {
            if (start_day == 0)
                count++;
            start_day = (start_day + month_days[x]) % 7;
        }
        return count + count_first_sundays(start_year+1, start_day);
    }

    euler_result run() {
        // Jan 1 1901 was a Tuesday.
        return count_first_sundays(1901, 2);
    }

    euler_entry entry(run);
}
