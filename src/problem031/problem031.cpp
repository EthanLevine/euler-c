#include <vector>

#include "runner.hpp"
#include "problem031.hpp"

namespace problem031 {
    const std::vector<uint32_t> denominations{
        200, 100, 50, 20, 10, 5, 2, 1
    };

    uint32_t count_combos(uint32_t total, uint32_t denom_index) {
        // handle edge cases (total==0 isn't strictly needed, but should
        // provide a small performance boost)
        if (denominations[denom_index] == 1)
            return 1;
        if (total == 0)
            return 1;
        // find largest number of denom coins we can use
        uint32_t denom = denominations[denom_index];
        uint32_t k = total / denom;
        uint32_t count = 0;
        for (uint32_t x = 0; x <= k; x++) {
            count += count_combos(total - denom*x, denom_index+1);
        }
        return count;
    }

    uint32_t count_combos(uint32_t total) {
        return count_combos(total, 0);
    }

    euler_result run() {
        const uint32_t total = 200;
        return count_combos(total);
    }

    euler_entry entry(run);
}
