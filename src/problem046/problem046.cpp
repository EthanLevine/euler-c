#include <cmath>
#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem046.hpp"

namespace problem046 {
    euler_result run() {
        const uint64_t limit = 100000;
        std::vector<bool> sieve = build_prime_sieve(limit);
        std::vector<uint64_t> primes = build_prime_vector(limit, sieve);
        
        // for all odd composites:
        for (uint64_t comp = 9; ; comp += 2) {
            if (!sieve[comp]) {
                // then, for all primes less than the composite number
                bool prime_found = false;
                for (auto it = primes.cbegin(); it != primes.cend() && *it < comp; ++it) {
                    // find the remainder
                    uint64_t rem = comp - *it;
                    // make sure its even
                    if (rem % 2 == 1)
                        continue;
                    // and test if its twice a perfect square
                    uint64_t root = (uint64_t)sqrt(rem/2);
                    if (root*root == rem/2) {
                        // we've found it
                        prime_found = true;
                        break;
                    }
                }
                // if we couldn't find a prime...
                if (!prime_found)
                    // we found the invalid composite number
                    return comp;
            }
        }
        // We couldn't find it (is "limit" too low?)
        return 0;
    }

    euler_entry entry(run);
}
