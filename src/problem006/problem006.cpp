#include "runner.hpp"
#include "problem006.hpp"

namespace problem006 {
    euler_result run() {
        const uint64_t maxnum = 100;
        uint64_t sum = 0;
        uint64_t sum_of_sq = 0;
        for (uint64_t x = 1; x <= maxnum; x++) {
            sum += x;
            sum_of_sq += x*x;
        }
        return (sum*sum)-sum_of_sq;
    }

    euler_entry entry(run);
}