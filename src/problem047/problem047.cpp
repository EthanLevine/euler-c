#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem047.hpp"

namespace problem047 {
    uint64_t count_distinct_prime_factors(std::vector<uint64_t>& primes, uint64_t num) {
        uint64_t num_factors = 0;
        for (uint64_t prime : primes) {
            if (num % prime == 0) {
                num_factors++;
                while (num % prime == 0)
                    num /= prime;
            }
            if (num == 1)
                break;
        }
        return num_factors;
    }

    euler_result run() {
        uint64_t limit = 100000;
        std::vector<uint64_t> primes = build_prime_vector(limit);
        for (uint64_t x = 1; ; x++) {
            if (count_distinct_prime_factors(primes, x) != 4)
                continue;
            x++;
            if (count_distinct_prime_factors(primes, x) != 4)
                continue;
            x++;
            if (count_distinct_prime_factors(primes, x) != 4)
                continue;
            x++;
            if (count_distinct_prime_factors(primes, x) != 4)
                continue;
            // we found 4 valid consecutive numbers
            // return the first
            return x-3;
        }
        // we couldn't find any
        // this makes no sense - the loop above should never terminate
        // except if an answer is found
        return 0;
    }

    euler_entry entry(run);
}
