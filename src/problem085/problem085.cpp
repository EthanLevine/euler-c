#include <cstdlib>

#include "runner.hpp"
#include "problem085.hpp"

namespace problem085 {
    int64_t count_rects(int64_t L, int64_t W) {
        return (L*(L+1))*(W*(W+1))/4;
    }

    euler_result run() {
        const int64_t target = 2000000;
        int64_t closest_count = 0;
        int64_t closest_area = 0;
        for (int64_t L = 2; ; L++) {
            for (int64_t W = L; ; W++) {
                int64_t count = count_rects(L, W);
                if (abs(target - closest_count) > abs(target - count)) {
                    closest_count = count;
                    closest_area = L*W;
                }
                if (count > target)
                    break;
            }
            if (count_rects(L, L) > target)
                break;
        }

        return closest_area;
    }

    euler_entry entry(run);
}
