#ifndef RUNNER_HPP
#define RUNNER_HPP
#include <functional>
#include <map>
#include <string>

class euler_result {
    int _result_type;
    int64_t _int;
    uint64_t _uint;
    double _double;
    std::string _string;
public:
    euler_result(int32_t result);
    euler_result(uint32_t result);
    euler_result(int64_t result);
    euler_result(uint64_t result);
    euler_result(double result);
    euler_result(std::string result);

    void print();
};

class euler_entry {
    std::function<euler_result()> _callable;
public:
    euler_entry(std::function<euler_result()> callable);
    euler_result operator()() const;
};
#endif