#include <cmath>
#include <fstream>
#include <string>
#include <vector>

#include "runner.hpp"
#include "problem042.hpp"

namespace problem042 {
    std::vector<std::string> parse_resource() {
        std::ifstream file_stream("resource/problem042_words.txt");
        std::vector<std::string> words;

        char word_buffer[30];
        while (file_stream.getline(word_buffer, 30, ',')) {
            std::string quoted_word(word_buffer);
            words.push_back(quoted_word.substr(1, quoted_word.size()-2));
        }

        return words;
    }

    uint32_t calc_word_score(std::string& word) {
        uint32_t score = 0;
        for (char c : word) {
            score += c - 'A' + 1;
        }
        return score;
    }

    bool is_triangular(uint32_t num) {
        // if sqrt(1 + 8*num) is an odd integer, num is triangular
        // (start with the formula for T(n) and solve for n)
        uint32_t square = 1 + 8*num;
        uint32_t root = (uint32_t)sqrt(square);
        return root*root == square && root % 2 == 1;
    }

    euler_result run() {
        uint32_t count = 0;
        std::vector<std::string> words = parse_resource();
        for (std::string& word : words) {
            if (is_triangular(calc_word_score(word)))
                count++;
        }
        return count;
    }

    euler_entry entry(run);
}
