#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem077.hpp"

namespace problem077 {
    uint32_t count_prime_combos(uint32_t num, std::vector<uint32_t>::const_iterator it) {
        if (num == 0)
            return 1;
        if (*it > num)
            return 0;
        uint32_t count = 0;
        while (true) {
            count += count_prime_combos(num, it+1);
            if (*it > num)
                break;
            else
                num -= *it;
        }
        return count;
    }

    uint32_t count_prime_combos(uint32_t num, std::vector<uint32_t>& primes) {
        return count_prime_combos(num, primes.cbegin());
    }

    euler_result run() {
        const uint32_t limit = 10000;
        const uint32_t target = 5000;
        std::vector<uint32_t> primes = build_prime_vector(limit);
        for (uint32_t x = 2; x < limit; x++) {
            uint32_t count = count_prime_combos(x, primes);
            if (count > target)
                return x;
        }
        return 0;
    }

    euler_entry entry(run);
}
