#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem045.hpp"

namespace problem045 {
    euler_result run() {
        for (uint64_t hex_index = 144; ; hex_index++) {
            uint64_t hex_number = hex_num(hex_index);
            if (is_tri_num(hex_number) && is_pent_num(hex_number))
                return hex_number;
        }
    }

    euler_entry entry(run);
}
