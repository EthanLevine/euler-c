#include "runner.hpp"
#include "problem001.hpp"

namespace problem001 {
    euler_result run() {
        int32_t sum = 0;
        for (int32_t i = 0; i < 1000; i++) {
            if (((i % 5) == 0) || ((i % 3) == 0))
                sum += i;
        }
        return sum;
    }
    euler_entry entry(run);
}