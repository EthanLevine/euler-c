#include "runner.hpp"
#include "problem028.hpp"

namespace problem028 {
    euler_result run() {
        uint64_t sum = 1; // start with middle 1
        uint64_t start = 1;
        for (uint64_t ring = 1; ring <= 500; ring++) {
            // the step size is ring*2;
            uint64_t step_size = ring*2;
            // add this ring to the sum
            sum += 4*start + 10*step_size;
            // set up 'start' for next ring
            start += 4*step_size;
        }
        return sum;
    }

    euler_entry entry(run);
}
