#include <vector>

#include "runner.hpp"
#include "problem043.hpp"

namespace problem043 {
    uint64_t concat_digits(std::vector<uint64_t>& digits, uint32_t start, uint32_t length) {
        uint64_t result = 0;
        for (uint32_t x = start; x < start + length; x++) {
            result *= 10;
            result += digits[x];
        }
        return result;
    }

    uint64_t try_input_permutation(std::vector<uint64_t>& digits) {
        bool valid = true;
        valid &= concat_digits(digits, 1, 3) % 2 == 0;
        valid &= concat_digits(digits, 2, 3) % 3 == 0;
        valid &= concat_digits(digits, 3, 3) % 5 == 0;
        valid &= concat_digits(digits, 4, 3) % 7 == 0;
        valid &= concat_digits(digits, 5, 3) % 11 == 0;
        valid &= concat_digits(digits, 6, 3) % 13 == 0;
        valid &= concat_digits(digits, 7, 3) % 17 == 0;
        if (valid)
            return concat_digits(digits, 0, 10);
        else
            return 0;
    }

    void swap_elems(std::vector<uint64_t>& vec, uint32_t a, uint32_t b) {
        uint64_t a_val = vec[a];
        vec[a] = vec[b];
        vec[b] = a_val;
    }

    uint64_t sum_all_input_permutations(std::vector<uint64_t>& digits, uint32_t index) {
        // if index == 9, try this permutation
        if (index == 9)
            return try_input_permutation(digits);
        // otherwise, we want to try swapping the 'index' element with each right element
        // including itself (no swap)
        uint64_t sum = 0;
        for (uint32_t swap_index = index; swap_index <= 9; swap_index++) {
            std::vector<uint64_t> digits_copy = digits;
            swap_elems(digits_copy, index, swap_index);
            sum += sum_all_input_permutations(digits_copy, index+1);
        }
        return sum;
    }

    uint64_t sum_all_input_permutations() {
        std::vector<uint64_t> digits_original {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        uint64_t sum = 0;
        for (uint32_t swap_index = 1; swap_index <= 9; swap_index++) {
            std::vector<uint64_t> digits_copy = digits_original;
            swap_elems(digits_copy, 0, swap_index);
            sum += sum_all_input_permutations(digits_copy, 1);
        }
        return sum;
    }

    euler_result run() {
        return sum_all_input_permutations();
    }

    euler_entry entry(run);
}
