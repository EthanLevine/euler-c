#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem041.hpp"

namespace problem041 {
    // returns 'true' if num is 1-n pandigital
    bool is_pandigital(uint64_t num) {
        std::vector<bool> digits(10);
        uint32_t digit_count = 0;
        while (num > 0) {
            uint64_t digit = num % 10;
            if (digit == 0)
                return false;
            if (digits[digit])
                return false;
            digits[digit] = true;
            num /= 10;
            digit_count++;
        }
        for (uint32_t x = 1; x <= digit_count; x++) {
            if (!digits[x])
                return false;
        }
        return true;
    }

    euler_result run() {
        std::vector<uint64_t> primes = build_prime_vector((uint64_t)1000000000);
        uint64_t largest_prime = 0;
        for (uint64_t prime : primes) {
            if (is_pandigital(prime))
                largest_prime = prime;
        }
        return largest_prime;
    }

    euler_entry entry(run);
}
