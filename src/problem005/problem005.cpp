#include <cmath>

#include "runner.hpp"
#include "problem005.hpp"

namespace problem005 {
    uint64_t gcd(uint64_t a, uint64_t b) {
        while (a != 0) {
            uint64_t temp = a;
            a = b % temp;
            b = temp;
        }
        return b;
    }

    uint64_t lcm(uint64_t a, uint64_t b) {
        return a*b/gcd(a,b);
    }

    euler_result run() {
        const uint64_t maxnum = 20;
        uint64_t result = 1;
        for (uint64_t x = 2; x <= maxnum; x++)
            result = lcm(result, x);
        return result;
    }

    euler_entry entry(run);
}