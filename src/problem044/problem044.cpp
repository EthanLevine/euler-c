#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem044.hpp"

namespace problem044 {
    euler_result run() {
        uint64_t min_diff = (int64_t)1 << 30;
        for (uint64_t a = 1; ; a++) {
            uint64_t a_pent = pent_num(a);
            if (pent_num(a+1)-a_pent > min_diff) {
                break;
            }
            for (uint64_t b = a+1; ; b++) {
                uint64_t b_pent = pent_num(b);
                if (b_pent - a_pent > min_diff) {
                    break;
                }
                if (is_pent_num(b_pent + a_pent) && is_pent_num(b_pent - a_pent)) {
                    min_diff = b_pent - a_pent;
                }
            }
        }
        return min_diff;
    }

    euler_entry entry(run);
}
