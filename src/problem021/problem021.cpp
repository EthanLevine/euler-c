#include <cmath>
#include <set>

#include "runner.hpp"
#include "problem021.hpp"

namespace problem021 {
    uint32_t sum_proper_divisors(uint32_t num) {
        uint32_t num_sqrt = (uint32_t)floor(sqrt(num));
        // start with 1
        uint32_t sum = 1;
        // add all factor pairs up to (not including) the sqrt
        for (uint32_t factor = 2; factor < num_sqrt; factor++) {
            if (num % factor == 0) {
                sum += factor + num/factor;
            }
        }
        // add sqrt for perfect squares
        if (num_sqrt*num_sqrt == num)
            sum += num_sqrt;
        return sum;
    }

    bool are_amicable(uint32_t a, uint32_t b) {
        // std::cout << "testing " << a << " and " << b << std::endl;
        return (a == sum_proper_divisors(b)) && (b == sum_proper_divisors(a));
    }

    euler_result run() {
        const uint32_t limit = 10000;
        // Make sure not to count amicable "triples" and more, if they exist
        std::set<uint32_t> amicable_numbers;
        for (uint32_t a = 2; a < limit; a++) {
            for (uint32_t b = a+1; b < limit; b++) {
                if (are_amicable(a, b)) {
                    amicable_numbers.insert(a);
                    amicable_numbers.insert(b);
                }
            }
        }
        uint32_t sum = 0;
        for (uint32_t x : amicable_numbers) {
            sum += x;
        }
        return sum;
    }

    euler_entry entry(run);
}
