#include <cmath>
#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem037.hpp"

namespace problem037 {
    bool is_trunc_prime_front(std::vector<bool>& prime_sieve, uint64_t num) {
        if (num < 10)
            return prime_sieve[num];
        else
            return prime_sieve[num] && is_trunc_prime_front(prime_sieve, num / 10);
    }

    bool is_trunc_prime_back(std::vector<bool>& prime_sieve, uint64_t num) {
        if (num < 10)
            return prime_sieve[num];
        else
            return prime_sieve[num] && is_trunc_prime_back(prime_sieve, num % (uint64_t)pow(10,floor(log10(num))));
    }

    bool is_trunc_prime(std::vector<bool>& prime_sieve, uint64_t num) {
        return is_trunc_prime_front(prime_sieve, num) && is_trunc_prime_back(prime_sieve, num);
    }

    euler_result run() {
        uint64_t limit = 10000000;
        std::vector<bool> prime_sieve = build_prime_sieve(limit);
        uint64_t sum = 0;
        uint32_t count = 0;
        for (uint64_t x = 10; count < 11 && x < limit; x++) {
            if (prime_sieve[x] && is_trunc_prime(prime_sieve, x)) {
                sum += x;
                count ++;
            }
        }
        return sum;
    }

    euler_entry entry(run);
}
