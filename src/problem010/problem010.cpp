#include <vector>
#include "runner.hpp"
#include "problem010.hpp"
#include "eulerlib.hpp"

namespace problem010 {
    euler_result run() {
        uint64_t limit = 2000000;
        std::vector<uint64_t> primes = build_prime_vector(limit);
        uint64_t sum = 0;
        for (uint64_t prime : primes) {
            sum += prime;
        }
        return sum;
    }

    euler_entry entry(run);
}