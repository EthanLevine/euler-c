#include <set>
#include <utility>
#include <vector>

#include "runner.hpp"
#include "problem079.hpp"

namespace problem079 {
    const std::vector<uint32_t> logins{
            319,
            680,
            180,
            690,
            129,
            620,
            762,
            689,
            762,
            318,
            368,
            710,
            720,
            710,
            629,
            168,
            160,
            689,
            716,
            731,
            736,
            729,
            316,
            729,
            729,
            710,
            769,
            290,
            719,
            680,
            318,
            389,
            162,
            289,
            162,
            718,
            729,
            319,
            790,
            680,
            890,
            362,
            319,
            760,
            316,
            729,
            380,
            319,
            728,
            716};

    std::set<std::pair<uint8_t, uint8_t>> get_dependencies(const std::vector<uint32_t>& logins) {
        std::set<std::pair<uint8_t, uint8_t>> deps;
        for (uint32_t login : logins) {
            uint8_t first = login / 100;
            uint8_t second = (login / 10) % 10;
            uint8_t third = login % 10;
            deps.insert({first, second});
            deps.insert({second, third});
        }
        return deps;
    }

    euler_result run() {
        std::set<std::pair<uint8_t, uint8_t>> deps = get_dependencies(logins);
        // assume (for this problem) that there are no repeats
        std::vector<uint8_t> combo;
        while (deps.size() > 0) {
            // find the number that only appears on left (first) side
            std::vector<bool> first_digits(10, false);
            std::vector<bool> second_digits(10, false);
            for (const std::pair<uint8_t, uint8_t>& dep : deps) {
                first_digits[dep.first] = true;
                second_digits[dep.second] = true;
            }
            for (uint8_t digit = 0; digit < 10; digit++) {
                if (first_digits[digit] && !second_digits[digit]) {
                    combo.push_back(digit);
                    for (auto it = deps.begin(); it != deps.end();) {
                        if (it->first == digit)
                            deps.erase(it++);
                        else
                            it++;
                    }
                }
            }
        }
        uint64_t combo_num = 0;
        for (uint8_t digit : combo) {
            combo_num *= 10;
            combo_num += digit;
        }
        return combo_num;
    }

    euler_entry entry(run);
}
