#ifndef EULERLIB_HPP
#define EULERLIB_HPP
#include <cmath>
#include <cstdint>
#include <vector>

template <class intType>
std::vector<bool> build_prime_sieve(intType limit) {
    std::vector<bool> sieve(limit);
    intType limit_sqrt = (intType)floor(sqrt(limit));

    for (intType x = 1; x <= limit_sqrt; x++) {
        for (intType y = 1; y <= limit_sqrt; y++) {
            intType n = 4*x*x + y*y;
            if (n <= limit && (n % 12 == 1 || n % 12 == 5))
                sieve[n].flip();

            n = 3*x*x + y*y;
            if (n <= limit && n % 12 == 7)
                sieve[n].flip();

            n = 3*x*x - y*y;
            if (x > y && n <= limit && n % 12 == 11)
                sieve[n].flip();
        }
    }

    // For all prime numbers, remove all multiples for its square
    for (intType n = 5; n <= limit_sqrt; n++) {
        if (sieve[n]) {
            intType n2 = n*n;
            for (intType k = n2; k <= limit; k += n2)
                sieve[k] = false;
        }
    }

    // Set small prime numbers
    sieve[2] = true;
    sieve[3] = true;
    return sieve;
}

template <class intType>
std::vector<intType> build_prime_vector(intType limit, std::vector<bool>& prime_sieve) {
    std::vector<intType> vec;
    vec.reserve((intType)((1.2 * limit) / log(limit)));
    for (intType x = 2; x <= limit; x++) {
        if (prime_sieve[x])
            vec.push_back(x);
    }
    vec.shrink_to_fit();
    return vec;
}

template <class intType>
std::vector<intType> build_prime_vector(intType limit) {
    std::vector<bool> prime_sieve = build_prime_sieve(limit);
    return build_prime_vector(limit, prime_sieve);
}

// Triangular numbers
uint32_t tri_num(uint32_t index);
uint64_t tri_num(uint64_t index);
bool is_tri_num(uint32_t num);
bool is_tri_num(uint64_t num);

// Pentagonal numbers
uint32_t pent_num(uint32_t index);
uint64_t pent_num(uint64_t index);
bool is_pent_num(uint32_t num);
bool is_pent_num(uint64_t num);

// Hexagonal numbers
uint32_t hex_num(uint32_t index);
uint64_t hex_num(uint64_t index);
bool is_hex_num(uint32_t num);
bool is_hex_num(uint64_t num);

// gcd functions
uint32_t gcd(uint32_t a, uint32_t b);
uint64_t gcd(uint64_t a, uint64_t b);
#endif