#include "runner.hpp"
#include "problem030.hpp"

namespace problem030 {
    euler_result run() {
        uint32_t pow5[10];
        uint32_t sum = 0;
        for (uint32_t x = 0; x <= 9; x++) {
            pow5[x] = x*x*x*x*x;
        }
        // I could use tighter bounds on this, but it doesn't really matter.
        for (uint32_t num = 10; num <= 400000; num++) {
            // calculate the digitsum
            uint32_t num_temp = num;
            uint32_t digit_power_sum = 0;
            while (num_temp > 0) {
                digit_power_sum += pow5[num_temp % 10];
                num_temp /= 10;
            }
            if (num == digit_power_sum)
                sum += num;
        }
        return sum;
    }

    euler_entry entry(run);
}
