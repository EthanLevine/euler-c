#include <cmath>
#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem069.hpp"

namespace problem069 {
    // NOTE: this function yields an INCORRECT VALUE if n is prime
    //       this is OKAY because prime numbers would have too low a quotient,
    //       and the error will only yield a smaller one
    double totient_quotient(uint64_t n, std::vector<uint64_t>& primes) {
        double denom = 1;
        uint64_t root_n = (uint64_t)floor(sqrt(n));
        for (uint64_t prime : primes) {
            if (prime > root_n)
                break;
            if (n % prime == 0) {
                denom *= prime - 1;
                denom /= prime;
                if (prime*prime != n) {
                    denom *= (n/prime) - 1;
                    denom /= (n/prime);
                }
            }
        }
        return 1/denom;
    }

    euler_result run() {
        const uint64_t limit = 999999;
        std::vector<uint64_t> primes = build_prime_vector(limit);
        uint64_t max_n = 0;
        double max_quotient = 0;
        for (uint64_t n = 2; n <= limit; n++) {
            double quotient = totient_quotient(n, primes);
            if (quotient > max_quotient) {
                max_quotient = quotient;
                max_n = n;
            }
        }
        return max_n;
    }

    euler_entry entry(run);
}
