#include <array>
#include <algorithm>
#include <vector>

#include "runner.hpp"
#include "problem068.hpp"

namespace problem068 {
    // fivegons are represented by 10-element arrays
    // the first 5 elements are the outer parts of the fivegon
    // the last 5 elements are the inner parts of the fivegon
    // outer element x is connected to inner element x+5
    // so a run of 3 elements is x, x+5, ((x+1)%5)+5

    uint64_t get_fivegon_value(const std::array<uint32_t, 10>& fivegon) {
        // outer values are in indexes 0-4
        uint32_t smallest_outer_index = 0;
        for (uint32_t outer_index = 1; outer_index < 5; outer_index++) {
            if (fivegon[outer_index] < fivegon[smallest_outer_index])
                smallest_outer_index = outer_index;
        }
        uint64_t result = 0;
        for (uint32_t inc = 0; inc < 5; inc++) {
            uint32_t outer_index = smallest_outer_index + inc;
            outer_index %= 5;
            result *= 1000;
            if (fivegon[outer_index] == 10)
                result *= 10;
            result += fivegon[outer_index] * 100;
            result += fivegon[outer_index+5] * 10;
            result += fivegon[((outer_index+1) % 5) + 5];
        }
        return result;
    }

    bool is_magic_fivegon(const std::array<uint32_t, 10>& fivegon) {
        uint32_t sum = fivegon[0] + fivegon[5] + fivegon[6];
        for (uint32_t outer = 1; outer < 5; outer++) {
            if (sum != (fivegon[outer] + fivegon[outer+5] + fivegon[((outer+1)%5)+5]))
                return false;
        }
        return true;
    }

    euler_result run() {
        // try almost all permutations
        std::array<uint32_t, 10> fivegon {10, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        uint64_t largest_fivegon_value = 0;
        do {
            if (is_magic_fivegon(fivegon)) {
                uint64_t fivegon_value = get_fivegon_value(fivegon);
                if (fivegon_value > largest_fivegon_value) {
                    largest_fivegon_value = fivegon_value;
                }
            }
        } while (std::next_permutation(fivegon.begin() + 1, fivegon.end()));
        return largest_fivegon_value;
    }

    euler_entry entry(run);
}
