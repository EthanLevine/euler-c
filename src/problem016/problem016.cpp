#include <gmpxx.h>
#include <string>

#include "runner.hpp"
#include "problem016.hpp"

namespace problem016 {
    euler_result run() {
        const uint32_t exponent = 1000;
        mpz_class result;
        mpz_class two(2);
        mpz_pow_ui(result.get_mpz_t(), two.get_mpz_t(), exponent);
        std::string result_str = result.get_str();
        uint32_t digit_sum = 0;
        for (char c : result_str) {
            digit_sum += c - '0';
        }
        return digit_sum;
    }

    euler_entry entry(run);
}
