#include <gmpxx.h>

#include "runner.hpp"
#include "problem056.hpp"

namespace problem056 {
    uint32_t calc_digit_sum(mpz_class num) {
        uint32_t sum = 0;
        while (num > 0) {
            mpz_class digit = num % 10;
            sum += digit.get_ui();
            num /= 10;
        }
        return sum;
    }
    euler_result run() {
        uint32_t max_sum = 0;
        for (uint32_t a = 1; a < 100; a++) {
            for (uint32_t b = 1; b < 100; b++) {
                mpz_class num;
                mpz_ui_pow_ui(num.get_mpz_t(), a, b);
                uint32_t sum = calc_digit_sum(num);
                if (sum > max_sum)
                    max_sum = sum;
            }
        }
        return max_sum;
    }

    euler_entry entry(run);
}
