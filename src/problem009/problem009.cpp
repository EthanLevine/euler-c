#include <cmath>
#include <cstring>

#include "runner.hpp"
#include "problem009.hpp"

namespace problem009 {
    euler_result run() {
        const uint32_t target = 1000;
        // use a "brute-force" approach
        for (uint32_t a = 1; a < target; a++) {
            for (uint32_t b = a+1; b < target; b++) {
                uint32_t c_sq = a*a + b*b;
                uint32_t c = (uint32_t)round(sqrt(c_sq));
                if (c*c == c_sq && a+b+c == target)
                    return a*b*c;
            }
        }
        return 0;
    }

    euler_entry entry(run);
}