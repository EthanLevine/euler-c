#include <gmpxx.h>
#include <string>

#include "runner.hpp"
#include "problem048.hpp"

namespace problem048 {
    mpz_class self_power_sum(uint32_t num) {
        mpz_class sum(0);
        for (uint32_t x = 1; x <= num; x++) {
            mpz_class self_power;
            mpz_ui_pow_ui(self_power.get_mpz_t(), x, x);
            sum += self_power;
        }
        return sum;
    }

    euler_result run() {
        const uint32_t limit = 1000;
        mpz_class result = self_power_sum(limit);
        mpz_class trunc_result;
        mpz_mod(trunc_result.get_mpz_t(), result.get_mpz_t(), (10000000000_mpz).get_mpz_t());
        return trunc_result.get_str();
    }

    euler_entry entry(run);
}
