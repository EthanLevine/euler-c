#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem050.hpp"

namespace problem050 {
    euler_result run() {
        // create the prime sieve
        uint32_t limit = 999999;
        std::vector<bool> sieve = build_prime_sieve(limit);
        // manually create a prime vector (we want to keep the sieve around)
        std::vector<uint32_t> prime_vector;
        for (uint32_t x = 2; x <= limit; x++) {
            if (sieve[x])
                prime_vector.push_back(x);
        }
        // now, for every prime number, find the longest consecutive sum of
        // prime numbers that is also prime, but under the limit.  Stop when the
        // sum passes the limit.
        uint32_t max_count = 0;
        uint32_t max_prime = 0;
        for (uint32_t start = 0; start < prime_vector.size(); start++) {
            uint32_t sum = prime_vector[start];
            uint32_t count = 1;
            for (uint32_t next = start+1; next < prime_vector.size(); next++) {
                count++;
                sum += prime_vector[next];
                if (sum > limit)
                    break;
                if (sieve[sum]) {
                    if (count > max_count) {
                        max_count = count;
                        max_prime = sum;
                    }
                }
            }
        }
        return max_prime;
    }

    euler_entry entry(run);
}
