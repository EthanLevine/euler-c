#include "runner.hpp"
#include "problem014.hpp"

namespace problem014 {
    uint64_t next_collatz(uint64_t num) {
        if (num % 2 == 0)
            return num / 2;
        else
            return num*3 + 1;
    }

    uint64_t collatz_size(uint64_t start) {
        uint64_t size = 0;
        for (uint64_t num = start; num != 1; num = next_collatz(num)) {
            size++;
        }
        return size;
    }

    euler_result run() {
        uint64_t max_start = 0;
        uint64_t max_size = 0;
        for (uint64_t x = 1; x < 1000000; x++) {
            uint64_t size = collatz_size(x);
            if (size > max_size) {
                max_size = size;
                max_start = x;
            }
        }
        return max_start;
    }

    euler_entry entry(run);
}
