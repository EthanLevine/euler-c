#include <vector>

#include "runner.hpp"
#include "problem015.hpp"

namespace problem015 {
    void count_paths(std::vector<uint64_t>& grid, uint32_t gridsize, uint32_t x, uint32_t y) {
        if (grid[gridsize*x+y] > 0) {
            return;
        } else if (x == gridsize-1 || y == gridsize-1) {
            grid[gridsize*x+y] = 1;
        } else {
            count_paths(grid, gridsize, x+1, y);
            count_paths(grid, gridsize, x, y+1);
            grid[gridsize*x+y] = grid[gridsize*(x+1)+y] + grid[gridsize*x+y+1];
        }
    }

    euler_result run() {
        // note:  +1 is there because the grid counts nodes, not squares
        const uint32_t gridsize = 21;
        std::vector<uint64_t> grid(gridsize*gridsize);
        count_paths(grid, gridsize, 0, 0);
        return grid[0];
    }

    euler_entry entry(run);
}
