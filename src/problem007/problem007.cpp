#include <cmath>

#include "runner.hpp"
#include "problem007.hpp"

namespace problem007 {
    bool is_prime(uint64_t num) {
        // tests if 'num' is prime using a rather brute-force test
        uint64_t max_factor = (uint64_t)floor(sqrt((double)num));
        for (uint64_t x = 2; x <= max_factor; x++) {
            if ((num % x) == 0)
                return false;
        }
        return true;
    }

    euler_result run() {
        const uint64_t prime_count_limit = 10001;
        uint64_t prime_count = 3; // we know 2, 3, and 5 are primes
        for (uint64_t x = 1; ; x++) {
            if (is_prime(x*6+1)) {
                prime_count++;
                if (prime_count == prime_count_limit)
                    return x*6+1;
            }
            if (is_prime(x*6+5)) {
                prime_count++;
                if (prime_count == prime_count_limit)
                    return x*6+5;
            }
        }
        return 0;
    }

    euler_entry entry(run);
}