#include <cmath>

#include "runner.hpp"
#include "problem004.hpp"

namespace problem004 {
    bool is_palindrome(uint64_t num, uint64_t digit_count) {
        // 'num' is a palindrome if:
        //  * its length is 1 (value between 0 and 9)
        //  * its length is 2 and is divisible by 11
        //  * the first and last digits are equal and the "inside" is
        //    a palindrome
        if (digit_count == 1)
            return true;
        if (digit_count == 2)
            return (num % 11) == 0;
        uint64_t last_digit = num % 10;
        uint64_t first_digit = num / (uint64_t)pow(10, digit_count-1);
        if (last_digit != first_digit) {
            return false;
        } else {
            uint64_t inside_part = (num / 10) % (uint64_t)pow(10, digit_count-2);
            return is_palindrome(inside_part, digit_count-2);
        }
    }

    bool is_palindrome(uint64_t num) {
        return is_palindrome(num, 1 + (uint64_t)floor(log10((double)num)));
    }

    euler_result run() {
        uint64_t largest_palindrome = 0;
        for (uint64_t x = 999; x >= 100; x--) {
            for (uint64_t y = x; y >= 100; y--) {
                uint64_t prod = x*y;
                if (is_palindrome(prod) && largest_palindrome < prod)
                    largest_palindrome = prod;
            }
        }
        return largest_palindrome;
    }

    euler_entry entry(run);
}