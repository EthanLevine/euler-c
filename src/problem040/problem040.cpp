#include <cmath>

#include "runner.hpp"
#include "problem040.hpp"

namespace problem040 {
    uint64_t get_digit(uint64_t position) {
        // base case
        if (position <= 9)
            return position;
        // figure out which segment this position is in
        // example segments:
        // 1: 1-9
        // 2: 10-189 (add 9*2*10^1)
        // 3: 190-2789 (add 9*3*10^2)
        // 4: 2790-38789 (add 9*4*10^3)
        uint64_t segment_pos = position;
        uint64_t segment = 1;
        while (true) {
            uint64_t segment_length = 9*segment*((uint64_t)pow(10,segment-1));
            if (segment_pos < segment_length)
                break;
            segment_pos -= segment_length;
            segment++;
        }
        // figure out which number the position falls in
        uint64_t num = (uint64_t)pow(10,segment-1) + ((segment_pos-1) / segment);
        // figure out the position's index into that number
        uint64_t index = segment - 1 - ((segment_pos-1) % segment);
        // extract the digit
        uint64_t mask = (uint64_t)pow(10,index);
        uint64_t digit = (num / mask) % 10;
        return digit;
    }
    euler_result run() {
        return get_digit(1)*get_digit(10)*get_digit(100)*get_digit(1000)*get_digit(10000)*get_digit(100000)*get_digit(1000000);
    }

    euler_entry entry(run);
}
