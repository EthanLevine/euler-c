#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "runner.hpp"
#include "problem054.hpp"

namespace problem054 {
    const uint8_t SUIT_DIAMONDS = 1;
    const uint8_t SUIT_HEARTS = 2;
    const uint8_t SUIT_SPADES = 3;
    const uint8_t SUIT_CLUBS = 4;

    const uint8_t HIGH_CARD = 1;
    const uint8_t ONE_PAIR = 2;
    const uint8_t TWO_PAIR = 3;
    const uint8_t THREE_OF_A_KIND = 4;
    const uint8_t STRAIGHT = 5;
    const uint8_t FLUSH = 6;
    const uint8_t FULL_HOUSE = 7;
    const uint8_t FOUR_OF_A_KIND = 8;
    const uint8_t STRAIGHT_FLUSH = 9;
    // ROYAL_FLUSH is part of STRAIGHT_FLUSH

    struct card {
        uint8_t value;
        uint8_t suit;

        std::string to_str() const {
            std::string result;
            switch (value) {
                case 10:
                    result.push_back('T');
                    break;
                case 11:
                    result.push_back('J');
                    break;
                case 12:
                    result.push_back('Q');
                    break;
                case 13:
                    result.push_back('K');
                    break;
                case 14:
                    result.push_back('A');
                    break;
                default:
                    if (value >= 2 && value <= 9)
                        result.push_back('0' + value);
            }
            switch (suit) {
                case SUIT_CLUBS:
                    result.push_back('C');
                    break;
                case SUIT_DIAMONDS:
                    result.push_back('D');
                    break;
                case SUIT_HEARTS:
                    result.push_back('H');
                    break;
                case SUIT_SPADES:
                    result.push_back('S');
                    break;
            }
            return result;
        }
    };

    struct hand {
        card cards[5];

        uint8_t rank;
        uint8_t value;

        std::string to_str() const {
            std::ostringstream oss;
            oss << "hand: "
                << cards[0].to_str()
                << " " << cards[1].to_str()
                << " " << cards[2].to_str()
                << " " << cards[3].to_str()
                << " " << cards[4].to_str()
                << ", rank: " << (uint32_t)rank
                << ", high: " << (uint32_t)value;
            return oss.str();
        }
    };

    void score_hand(hand& hand) {
        // first, sort the cards in DESCENDING order
        std::sort(hand.cards, hand.cards+5, [](card a, card b){return a.value >= b.value;});
        // now, let's see if this is a flush
        bool is_flush = true;
        for (uint8_t idx = 1; idx < 5; idx++) {
            is_flush &= hand.cards[idx].suit == hand.cards[0].suit;
        }
        // let's see if this is a straight
        bool is_straight = true;
        for (uint8_t idx = 0; idx < 4; idx++) {
            is_straight &= hand.cards[idx].value == hand.cards[idx+1].value + 1;
        }
        // If this is a flush and a straight, we have a straight flush!
        if (is_flush && is_straight) {
            hand.rank = STRAIGHT_FLUSH;
            hand.value = hand.cards[0].value;
            return;
        }
        // If not, let's try for 4-of-a-kind
        // the middle 3 cards must be equal, and 1 of the end cards
        if (hand.cards[1].value == hand.cards[2].value && hand.cards[2].value == hand.cards[3].value
                && (hand.cards[1].value == hand.cards[0].value || hand.cards[1].value == hand.cards[4].value)) {
            // 4-of-a-kind
            hand.rank = FOUR_OF_A_KIND;
            hand.value = hand.cards[1].value;
            return;
        }
        // Well, it could be full house
        // first, see if it's a 3-2 full house
        if (hand.cards[0].value == hand.cards[1].value
                && hand.cards[1].value == hand.cards[2].value
                && hand.cards[3].value == hand.cards[4].value) {
            hand.rank = FULL_HOUSE;
            hand.value = hand.cards[0].value;
            return;
        }
        // or a 2-3 full house (2 big cards, 3 small cards)
        if (hand.cards[0].value == hand.cards[1].value
                && hand.cards[2].value == hand.cards[3].value
                && hand.cards[3].value == hand.cards[4].value) {
            hand.rank = FULL_HOUSE;
            hand.value = hand.cards[2].value;
            return;
        }
        // check for flushes and straights
        if (is_flush) {
            hand.rank = FLUSH;
            hand.value = hand.cards[0].value;
            return;
        }
        if (is_straight) {
            hand.rank = STRAIGHT;
            hand.value = hand.cards[0].value;
            return;
        }
        // check for 3-of-a-kind
        for (uint8_t idx = 0; idx < 3; idx++) {
            if (hand.cards[idx].value == hand.cards[idx+1].value && hand.cards[idx].value == hand.cards[idx+2].value) {
                hand.rank = THREE_OF_A_KIND;
                hand.value = hand.cards[idx].value;
                return;
            }
        }
        // find the first pair
        uint8_t pair_idx;
        for (pair_idx = 0; pair_idx < 4; pair_idx++) {
            if (hand.cards[pair_idx].value == hand.cards[pair_idx+1].value)
                break;
        }
        // if no pairs were found, it's a high card
        if (pair_idx == 4) {
            hand.rank = HIGH_CARD;
            hand.value = hand.cards[0].value;
            return;
        }
        // try finding a second pair
        for (uint8_t pair_idx2 = pair_idx+1; pair_idx2 < 4; pair_idx2++) {
            if (hand.cards[pair_idx2].value == hand.cards[pair_idx2+1].value) {
                // 2 pair
                hand.rank = TWO_PAIR;
                hand.value = hand.cards[pair_idx].value;
                return;
            }
        }
        // no second pair, so just the one
        hand.rank = ONE_PAIR;
        hand.value = hand.cards[pair_idx].value;
        return;
    }

    hand read_hand(const char *cstr) {
        hand hand;
        for (uint8_t idx = 0; idx < 5; idx++) {
            switch (cstr[idx*3]) {
                case 'T':
                    hand.cards[idx].value = 10;
                    break;
                case 'J':
                    hand.cards[idx].value = 11;
                    break;
                case 'Q':
                    hand.cards[idx].value = 12;
                    break;
                case 'K':
                    hand.cards[idx].value = 13;
                    break;
                case 'A':
                    hand.cards[idx].value = 14;
                    break;
                default:
                    hand.cards[idx].value = cstr[idx*3] - '0';
            }
            switch (cstr[idx*3+1]) {
                case 'D':
                    hand.cards[idx].suit = SUIT_DIAMONDS;
                    break;
                case 'H':
                    hand.cards[idx].suit = SUIT_HEARTS;
                    break;
                case 'S':
                    hand.cards[idx].suit = SUIT_SPADES;
                    break;
                case 'C':
                    hand.cards[idx].suit = SUIT_CLUBS;
                    break;
            }
        }
        return hand;
    }

    std::vector<std::pair<hand, hand>> parse_resource() {
        std::ifstream file_stream("resource/problem054_poker.txt");
        const size_t LINE_SIZE = 30;
        char line[LINE_SIZE];
        std::vector<std::pair<hand, hand>> pairs;

        while (!file_stream.eof()) {
            file_stream.read(line, LINE_SIZE);
            if ((size_t)file_stream.gcount() >= LINE_SIZE - 1)
                pairs.push_back({read_hand(line), read_hand(line+15)});
        }

        return pairs;
    }

    bool does_p1_win(const hand& p1, const hand& p2) {
        // check score
        if (p1.rank > p2.rank)
            return true;
        if (p1.rank < p2.rank)
            return false;
        // check score value
        if (p1.value > p2.value)
            return true;
        if (p1.value < p2.value)
            return false;
        // check cards (in order)
        for (uint8_t idx = 0; idx < 5; idx++) {
            if (p1.cards[idx].value > p2.cards[idx].value)
                return true;
            if (p1.cards[idx].value < p2.cards[idx].value)
                return false;
        }
        // should never reach here
        // unless the hands are IDENTICAL in value
        // let's just say that in a tie, EVERYONE loses
        return false;
    }

    euler_result run() {
        std::vector<std::pair<hand, hand>> pairs = parse_resource();
        uint32_t count = 0;
        for (std::pair<hand, hand>& hands : pairs) {
            score_hand(hands.first);
            score_hand(hands.second);
            if (does_p1_win(hands.first, hands.second)) {
                count++;
            }
        }
        return count;
    }

    euler_entry entry(run);
}
