#include <fstream>
#include <utility>
#include <vector>

#include "runner.hpp"
#include "problem082.hpp"

namespace problem082 {
    const uint32_t matrix_size = 80;
    
    uint32_t get(const std::vector<uint32_t>& matrix, uint32_t row, uint32_t col) {
        return matrix[row*matrix_size + col];
    }

    void set(std::vector<uint32_t>& matrix, uint32_t row, uint32_t col, uint32_t val) {
        matrix[row*matrix_size + col] = val;
    }

    uint32_t get(const std::vector<uint32_t>& matrix, const std::pair<uint32_t, uint32_t>& pos) {
        return matrix[pos.first*matrix_size + pos.second];
    }

    void set(std::vector<uint32_t>& matrix, const std::pair<uint32_t, uint32_t>& pos, uint32_t val) {
        matrix[pos.first*matrix_size + pos.second] = val;
    }

    std::vector<uint32_t> parse_resource() {
        std::ifstream file_stream("resource/problem081_082_083_matrix.txt");
        std::vector<uint32_t> matrix(matrix_size*matrix_size);
        for (uint32_t row = 0; row < matrix_size; row++) {
            for (uint32_t col = 0; col < matrix_size; col++) {
                uint32_t val;
                file_stream >> val;
                if (col != matrix_size-1) {
                    char comma;
                    file_stream >> comma;
                }
                set(matrix, row, col, val);
            }
        }
        return matrix;
    }

    uint32_t get_min_path(const std::vector<uint32_t>& matrix) {
        const uint32_t max_cost = 1 << 30;
        std::vector<uint32_t> costs(matrix_size*matrix_size, max_cost);
        // set the last column to 'costs' to 'matrix'
        for (uint32_t row = 0; row < matrix_size; row++) {
            set(costs, row, matrix_size-1, get(matrix, row, matrix_size-1));
        }
        // for all other columns (going backwards)...
        for (uint32_t col = matrix_size-2; ; col--) {
            // Sweep down and then up the column, finding the smallest cost
            set(costs, 0, col, get(costs, 0, col+1) + get(matrix, 0, col));
            for (uint32_t row = 1; row < matrix_size; row++) {
                uint32_t up_cost = get(costs, row-1, col);
                uint32_t right_cost = get(costs, row, col+1);
                set(costs, row, col, get(matrix, row, col) + (up_cost > right_cost ? right_cost : up_cost));
            }
            for (uint32_t row = matrix_size-2; ; row--) {
                uint32_t down_cost = get(costs, row+1, col) + get(matrix, row, col);
                uint32_t current_cost = get(costs, row, col);
                if (down_cost < current_cost)
                    set(costs, row, col, down_cost);
                if (row == 0)
                    break;
            }
            if (col == 0)
                break;
        }
        // find the smallest cost in the first column
        uint32_t min_cost = max_cost;
        for (uint32_t row = 0; row < matrix_size; row++) {
            uint32_t cost = get(costs, row, 0);
            if (cost < min_cost)
                min_cost = cost;
        }
        return min_cost;
    }

    euler_result run() {
        const std::vector<uint32_t> matrix = parse_resource();
        return get_min_path(matrix);
    }

    euler_entry entry(run);
}
