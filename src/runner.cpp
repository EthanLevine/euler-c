#include <iostream>
#include <functional>
#include <map>
#include <string>
#include <utility>

#include <cstdlib>
#include <cstring>

#include "runner.hpp"
#include "runner.headers.inc"

#define TYPE_INT      1
#define TYPE_UINT     2
#define TYPE_DOUBLE   3
#define TYPE_STRING   4

euler_entry::euler_entry(std::function<euler_result()> callable) :
        _callable(callable) {}
euler_result euler_entry::operator()() const { return _callable(); }


euler_result::euler_result(int32_t result) :
        _result_type(TYPE_INT), _int(result) {}
euler_result::euler_result(uint32_t result) :
        _result_type(TYPE_UINT), _uint(result) {}
euler_result::euler_result(int64_t result) :
        _result_type(TYPE_INT), _int(result) {}
euler_result::euler_result(uint64_t result) :
        _result_type(TYPE_UINT), _uint(result) {}
euler_result::euler_result(double result) :
        _result_type(TYPE_DOUBLE), _double(result) {}
euler_result::euler_result(std::string result) :
        _result_type(TYPE_STRING), _string(result) {}

void euler_result::print() {
    if (_result_type == TYPE_INT) {
        std::cout << _int;
    } else if (_result_type == TYPE_UINT) {
        std::cout << _uint;
    } else if (_result_type == TYPE_DOUBLE) {
        std::cout << _double;
    } else if (_result_type == TYPE_STRING) {
        std::cout << _string;
    } else {
        throw std::exception();
    }
}

void usage() {

}

void help() {

}

int main(int argc, char **argv) {
    // call syntax:
    // euler <command>
    //   OR
    // euler <problem #> [arg1=value1, arg2=value2, ...]
    //
    // <command> is one of:
    //  * help
    //  * usage
    
    // build problem map
    std::map<long, euler_entry> problem_entries{
#include "runner.problems.inc"
    };

    // make sure we have enough arguments (at least 1)
    if (argc < 2) {
        usage();
        return 1;
    }

    if (strcmp(argv[1], "usage") == 0) {
        usage();
        return 0;
    }

    if (strcmp(argv[1], "help") == 0) {
        help();
        return 0;
    }

    // todo: add "args" command to print args of a problem
    errno = 0;
    char *endptr;
    long problem_number = strtol(argv[1], &endptr, 10);
    if (*endptr == '\0' && endptr != argv[1] && errno == 0) {
        // no error, problem_number is valid
        std::cout << "Loading problem " << problem_number << "..." << std::endl;
        euler_entry& entry = problem_entries.find(problem_number)->second;
        std::cout << "Running problem " << problem_number << "..." << std::endl;
        euler_result result = entry();
        std::cout << "Result: ";
        result.print();
        std::cout << std::endl;
        return 0;
    } else {
        // invalid arguments
        std::cout << "Invalid arguments" << std::endl;
        usage();
        return 1;
    }
}