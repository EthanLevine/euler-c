#include <gmpxx.h>
#include <set>

#include "runner.hpp"
#include "problem029.hpp"

namespace problem029 {
    euler_result run() {
        std::set<mpz_class> terms;
        for (uint32_t a = 2; a <= 100; a++) {
            for (uint32_t b = 2; b <= 100; b++) {
                mpz_class a_mpz(a);
                mpz_class term;
                mpz_pow_ui(term.get_mpz_t(), a_mpz.get_mpz_t(), b);
                terms.insert(term);
            }
        }
        return terms.size();
    }

    euler_entry entry(run);
}
