#include <vector>

#include "runner.hpp"
#include "problem174.hpp"

namespace problem174 {
    uint32_t count_combinations(uint64_t total) {
        if (total % 4 != 0)
            return 0;
        const uint64_t quarter_total = total/4;
        uint32_t count = 0;
        // try odd rings
        uint64_t low = 2;
        uint64_t high = 0;
        uint64_t sum = 0;
        do {
            if (sum == quarter_total)
                count++;
            if (sum < quarter_total) {
                high += 2;
                sum += high;
            } else {
                sum -= low;
                low += 2;
            }
        } while (low <= high);
        // try even rings
        low = 3;
        high = 1;
        sum = 0;
        do {
            if (sum == quarter_total)
                count++;
            if (sum < quarter_total) {
                high += 2;
                sum += high;
            } else {
                sum -= low;
                low += 2;
            }
        } while (low <= high);
        return count;
    }

    euler_result run() {
        const uint64_t limit = 1000000;
        const uint32_t min_count = 1;
        const uint32_t max_count = 10;
        uint32_t total = 0;
        for (uint64_t x = 8; x <= limit; x += 4) {
            uint32_t count = count_combinations(x);
            if (count >= min_count && count <= max_count)
                total++;
        }
        return total;
    }

    euler_entry entry(run);
}
