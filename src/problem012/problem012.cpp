#include <cmath>

#include "runner.hpp"
#include "problem012.hpp"

namespace problem012 {
    uint64_t count_factors(uint64_t num) {
        uint64_t num_root = (uint64_t)floor(sqrt(num));
        uint64_t count = 2; // start with 1 and num
        for (uint64_t factor = 2; factor <= num_root; factor++) {
            if (num % factor == 0)
                count += 2;
        }
        // remove a factor if num is a perfect square
        if (num_root * num_root == num)
            count -= 1;
        return count;
    }

    euler_result run() {
        const uint64_t divcount = 500;
        uint64_t triangle_num = 0;
        for (uint64_t x = 1; ; x++) {
            triangle_num += x;
            if (count_factors(triangle_num) > divcount)
                return triangle_num;
        }
        // never reached
        return 0;
    }

    euler_entry entry(run);
}
