#include <cmath>
#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem035.hpp"

namespace problem035 {
    uint64_t rotate(uint64_t num, uint64_t mod) {
        return (num / 10) + mod * (num % 10);
    }

    bool is_circular_prime(std::vector<bool>& sieve, uint64_t num, uint32_t rotations, uint64_t mod) {
        // if we're done rotating, return true
        if (rotations == 0)
            return true;
        // if num isn't prime, return false
        if (!sieve[num])
            return false;
        // return whatever the next is_circular_prime() call yields
        return is_circular_prime(sieve, rotate(num, mod), rotations-1, mod);
    }

    bool is_circular_prime(std::vector<bool>& sieve, uint64_t prime) {
        // calculate the number of digits in prime
        uint32_t num_digits = 1 + (uint32_t)floor(log10(prime));
        // calculate the modulus (10^(digits-1))
        uint64_t mod = (uint64_t)pow(10, num_digits-1);
        // call helper function
        return is_circular_prime(sieve, rotate(prime, mod), num_digits-1, mod);
    }

    euler_result run() {
        const uint64_t limit = 999999;
        // first, get all primes below 1,000,000
        std::vector<bool> sieve = build_prime_sieve(limit);
        // count the number of them that are circular
        uint32_t count = 0;
        for (uint64_t num = 2; num < limit; num++) {
            if (sieve[num] && is_circular_prime(sieve, num))
                count++;
        }
        return count;
    }

    euler_entry entry(run);
}
