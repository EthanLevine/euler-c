#include <cmath>
#include <fstream>
#include <string>

#include "runner.hpp"
#include "problem099.hpp"

namespace problem099 {
    bool is_pair_greater_than(uint32_t base1, uint32_t expo1, uint32_t base2, uint32_t expo2) {
        double value2 = pow(base2, (double)expo2 / expo1);
        return base1 > value2;
    }

    void parse_base_expo_pair(std::ifstream& stream, uint32_t& base, uint32_t& expo) {
        std::string base_str;
        std::string expo_str;
        std::getline(stream, base_str, ',');
        std::getline(stream, expo_str);
        base = stoul(base_str);
        expo = stoul(expo_str);
    }

    euler_result run() {
        std::ifstream file_stream("resource/problem099_base_exp.txt");
        uint32_t max_base = 0;
        uint32_t max_expo = 1;
        uint32_t max_line_num = 0;
        for (uint32_t line_num = 1; ; line_num++) {
            if (file_stream.eof())
                break;
            uint32_t base;
            uint32_t expo;
            parse_base_expo_pair(file_stream, base, expo);
            if (is_pair_greater_than(base, expo, max_base, max_expo)) {
                max_base = base;
                max_expo = expo;
                max_line_num = line_num;
            }
            if (file_stream.eof())
                break;
        }
        return max_line_num;
    }

    euler_entry entry(run);
}
