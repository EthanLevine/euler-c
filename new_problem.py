import sys
import os

hpp_template = """
#ifndef %s
#define %s
#include "runner.hpp"

namespace %s {
    extern euler_entry entry;
}
#endif
"""

cpp_template = """
#include "runner.hpp"
#include "%s.hpp"

namespace %s {
    euler_result run() {
        // write code here
    }

    euler_entry entry(run);
}
"""

problem_number = int(sys.argv[1])
problem_str = "problem%03i" % problem_number
header_guard = "PROBLEM%03i_HPP" % problem_number
os.chdir("src")
os.mkdir(problem_str)
open("runner.problems.inc", "a").write("\n{%i, %s::entry}," % (problem_number, problem_str))
open("runner.headers.inc", "a").write("\n#include \"%s/%s.hpp\"" % (problem_str, problem_str))
open("%s/%s.hpp" % (problem_str, problem_str), "w").write(hpp_template % (header_guard, header_guard, problem_str))
open("%s/%s.cpp" % (problem_str, problem_str), "w").write(cpp_template % (problem_str, problem_str))
