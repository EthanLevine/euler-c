#include <gmpxx.h>
#include <string>

#include "runner.hpp"
#include "problem020.hpp"

namespace problem020 {
    euler_result run() {
        mpz_class result;
        mpz_fac_ui(result.get_mpz_t(), 100);
        std::string result_str = result.get_str();
        uint64_t digit_sum = 0;
        for (char c : result_str) {
            digit_sum += c - '0';
        }
        return digit_sum;
    }

    euler_entry entry(run);
}
