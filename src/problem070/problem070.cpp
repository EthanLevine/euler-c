#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem070.hpp"

namespace problem070 {
    uint64_t totient(uint64_t n, std::vector<uint64_t>& primes) {
        uint64_t result = n;
        uint64_t root_n = (uint64_t)floor(sqrt(n));
        for (uint64_t prime : primes) {
            if (prime > root_n)
                break;
            if (n % prime == 0) {
                result *= prime - 1;
                result /= prime;
                if (prime*prime != n) {
                    result *= (n/prime) - 1;
                    result /= (n/prime);
                }
            }
        }
        // if n is prime, then result == n, because no other prime factors
        // have been found yet
        if (result == n)
            result = n - 1;
        return result;
    }

    bool is_permutation(uint64_t a, uint64_t b) {
        int8_t digits[10] = {0};
        while (a > 0) {
            digits[a % 10]++;
            a /= 10;
        }
        while (b > 0) {
            digits[b % 10]--;
            b /= 10;
        }
        // make sure digits is all 0's
        for (uint8_t idx = 0; idx < 10; idx++) {
            if (digits[idx] != 0)
                return false;
        }
        return true;
    }

    euler_result run() {
        const uint64_t limit = 9999999;
        std::vector<uint64_t> primes = build_prime_vector(limit);
        double min_ratio = 100;
        uint64_t chosen_x = 0;
        for (uint64_t x = 2; x < limit; x++) {
            uint64_t t = totient(x, primes);
            double ratio = (double)x / t;
            if (ratio < min_ratio && is_permutation(x, t)) {
                min_ratio = ratio;
                chosen_x = x;
            }
        }
        return chosen_x;
    }

    euler_entry entry(run);
}
