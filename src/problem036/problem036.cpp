#include <vector>

#include "runner.hpp"
#include "problem036.hpp"

namespace problem036 {
    bool is_palindrome(uint32_t num, uint32_t base) {
        // note: 'digits' is reversed
        std::vector<uint32_t> digits;
        while (num > 0) {
            digits.push_back(num % base);
            num /= base;
        }
        // note: digits should have AT LEAST one member!
        // (this function breaks with num == 0)
        for (auto it_f = digits.cbegin(), it_b = digits.cend()-1; it_f != it_b && it_f != it_b+1; ++it_f, --it_b) {
            if (*it_f != *it_b)
                return false;
        }
        return true;
    }

    euler_result run() {
        uint64_t sum = 0;
        for (uint32_t x = 1; x < 1000000; x++) {
            if (is_palindrome(x, 10) && is_palindrome(x, 2))
                sum += x;
        }
        return sum;
    }

    euler_entry entry(run);
}
