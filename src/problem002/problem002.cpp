#include "runner.hpp"
#include "problem002.hpp"

namespace problem002 {
    euler_result run() {
        const uint32_t limit = 4000000;
        uint32_t fib_a = 1;
        uint32_t fib_b = 1;
        uint32_t even_sum = 0;
        while (fib_b < limit) {
            if ((fib_b % 2) == 0)
                even_sum += fib_b;
            uint32_t fib_temp = fib_b;
            fib_b += fib_a;
            fib_a = fib_temp;
        }
        return even_sum;
    }

    euler_entry entry(run);
}