#include <algorithm>
#include <cmath>
#include <vector>

#include "runner.hpp"
#include "problem170.hpp"

namespace problem170 {
    bool include_digits(std::vector<bool>& digits, uint64_t num) {
        // special case for num == 0
        if (num == 0) {
            if (digits[0])
                return false;
            digits[0] = true;
            return true;
        }
        while (num > 0) {
            uint64_t digit = num % 10;
            if (digits[digit])
                return false;
            else
                digits[digit] = true;
            num /= 10;
        }
        return true;
    }

    uint64_t concat(uint64_t a, uint64_t b) {
        // find #digits in b
        uint64_t b_mask = (uint64_t)pow(10,floor(log10(b))+1);
        return a*b_mask + b;
    }

    uint64_t try_get_pandigital(uint64_t x, uint64_t a, uint64_t b) {
        std::vector<bool> digits(10);
        uint64_t ax = a*x;
        uint64_t bx = b*x;
        if (!include_digits(digits, ax) || !include_digits(digits, bx))
            return 0;
        return concat(ax, bx);
    }

    uint64_t try_get_pandigital(uint64_t x, uint64_t a, uint64_t b, uint64_t c) {
        std::vector<bool> digits(10);
        uint64_t ax = a*x;
        uint64_t bx = b*x;
        uint64_t cx = c*x;
        if (!include_digits(digits, ax) || !include_digits(digits, bx) || !include_digits(digits, cx))
            return 0;
        return concat(ax, concat(bx, cx));
    }

    uint64_t try_get_pandigital(uint64_t x, uint64_t a, uint64_t b, uint64_t c, uint64_t d) {
        std::vector<bool> digits(10);
        uint64_t ax = a*x;
        uint64_t bx = b*x;
        uint64_t cx = c*x;
        uint64_t dx = d*x;
        if (!include_digits(digits, ax) || !include_digits(digits, bx) || !include_digits(digits, cx) || !include_digits(digits, dx))
            return 0;
        return concat(ax, concat(bx, concat(cx, dx)));
    }

    uint64_t try_get_pandigital(uint64_t x, uint64_t a, uint64_t b, uint64_t c, uint64_t d, uint64_t e) {
        std::vector<bool> digits(10);
        uint64_t ax = a*x;
        uint64_t bx = b*x;
        uint64_t cx = c*x;
        uint64_t dx = d*x;
        uint64_t ex = e*x;
        if (!include_digits(digits, ax) || !include_digits(digits, bx) || !include_digits(digits, cx) || !include_digits(digits, dx) || !include_digits(digits, ex))
            return 0;
        return concat(ax, concat(bx, concat(cx, concat(dx, ex))));
    }

    // New idea:
    // For every permutation of 0123456789:
    //     For every 3-partition:
    //         Discard if any partition starts with 0
    //         Calculate the product
    //         Determine if pandigital
    //     For every 4-partition:
    //         (above)
    //     ...
    //     For every 9-partition:
    //         (above)
    //
    // There are only 10! = 3,628,800 permutations
    // There are 36 3-partitions...
    // ...
    // There has to be a better way to do the partitioning
    // when two numbers are multiplied, their product must have:
    //   AT LEAST as many digits as the sum of the multiplicand digit counts minus 1
    //   AT MOST as many digits as the sum of the multiplicand digit counts
    // So, for 3-partitions, x may have 1-2 digits
    // For 4-partitions and up, x may have 1 digit only
    // Also, x can never be 1
    // Knowing that, the new partition counts are:
    // N    count
    // 3    15
    // 4    28
    // 5    125
    // 6    ...
    //
    // So, new algorithm only uses permutations starting with a 2-9.
    // It will always take only one digit for x.
    // The nary-partitions will be hardcoded as a series of nested for loops
    uint64_t concat_digits(std::vector<uint64_t>& digits, uint32_t start, uint32_t length) {
        uint64_t result = 0;
        for (uint32_t x = start; x < start + length; x++) {
            result *= 10;
            result += digits[x];
        }
        return result;
    }

    uint64_t try_input_permutation(std::vector<uint64_t>& digits) {
        // digits[0] must not be 0
        if (digits[0] == 0)
            return 0;

        uint64_t max = 0;

        // if digits[0] is 2 or more, try a 1-digit x, but only if the second digit
        // is not 0
        if (digits[0] >= 2 && digits[1] > 0) {
            // try with 2 multipliers
            for (uint32_t a_len = 1; a_len < 8; a_len++) {
                if (digits[1+a_len] == 0)
                    continue;
                uint32_t b_len = 9-a_len;
                uint64_t test_max = try_get_pandigital(
                    digits[0],
                    concat_digits(digits, 1, a_len),
                    concat_digits(digits, 1+a_len, b_len));
                if (test_max > max)
                    max = test_max;
            }
            // try with 3 multipliers
            for (uint32_t a_len = 1; a_len < 7; a_len++) {
                if (digits[1+a_len] == 0)
                    continue;
                uint64_t a = concat_digits(digits, 1, a_len);
                for (uint32_t b_len = 1; b_len < 8-a_len; b_len++) {
                    if (digits[1+a_len+b_len] == 0)
                        continue;
                    uint32_t c_len = 9-a_len-b_len;
                    uint64_t test_max = try_get_pandigital(
                        digits[0],
                        a,
                        concat_digits(digits, 1+a_len, b_len),
                        concat_digits(digits, 1+a_len+b_len, c_len));
                    if (test_max > max)
                        max = test_max;
                }
            }
            // try with 4 multipliers
            for (uint32_t a_len = 1; a_len < 6; a_len++) {
                if (digits[1+a_len] == 0)
                    continue;
                uint64_t a = concat_digits(digits, 1, a_len);
                for (uint32_t b_len = 1; b_len < 7-a_len; b_len++) {
                    if (digits[1+a_len+b_len] == 0)
                        continue;
                    uint64_t b = concat_digits(digits, 1+a_len, b_len);
                    for (uint32_t c_len = 1; c_len < 8-a_len-b_len; c_len++) {
                        if (digits[1+a_len+b_len+c_len] == 0)
                            continue;
                        uint32_t d_len = 9-a_len-b_len-c_len;
                        uint64_t test_max = try_get_pandigital(
                            digits[0],
                            a,
                            b,
                            concat_digits(digits, 1+a_len+b_len, c_len),
                            concat_digits(digits, 1+a_len+b_len+c_len, d_len));
                        if (test_max > max)
                            max = test_max;
                    }
                }
            }
            // try with 5 multipliers
            for (uint32_t a_len = 1; a_len < 5; a_len++) {
                if (digits[1+a_len] == 0)
                    continue;
                uint64_t a = concat_digits(digits, 1, a_len);
                for (uint32_t b_len = 1; b_len < 6-a_len; b_len++) {
                    if (digits[1+a_len+b_len] == 0)
                        continue;
                    uint64_t b = concat_digits(digits, 1+a_len, b_len);
                    for (uint32_t c_len = 1; c_len < 7-a_len-b_len; c_len++) {
                        if (digits[1+a_len+b_len+c_len] == 0)
                            continue;
                        uint64_t c = concat_digits(digits, 1+a_len+b_len, c_len);
                        for (uint32_t d_len = 1; d_len < 8-a_len-b_len-c_len; d_len++) {
                            if (digits[1+a_len+b_len+c_len+d_len] == 0)
                                continue;
                            uint32_t e_len = 9-a_len-b_len-c_len-d_len;
                            uint64_t test_max = try_get_pandigital(
                                digits[0],
                                a,
                                b,
                                c,
                                concat_digits(digits, 1+a_len+b_len+c_len, d_len),
                                concat_digits(digits, 1+a_len+b_len+c_len+d_len, e_len));
                            if (test_max > max)
                                max = test_max;
                        }
                    }
                }
            }
        }

        // try a 2-digit x, but only with 2 multipliers
        // and only if the third digit is not 0
        if (digits[2] > 0) {
            uint64_t x = concat_digits(digits, 0, 2);
            for (uint32_t a_len = 1; a_len < 7; a_len++) {
                if (digits[2+a_len] == 0)
                    continue;
                uint32_t b_len = 8-a_len;
                uint64_t test_max = try_get_pandigital(
                    x,
                    concat_digits(digits, 2, a_len),
                    concat_digits(digits, 2+a_len, b_len));
                if (test_max > max)
                    max = test_max;
            }
        }
        return max;
    }

    void swap_elems(std::vector<uint64_t>& vec, uint32_t a, uint32_t b) {
        uint64_t a_val = vec[a];
        vec[a] = vec[b];
        vec[b] = a_val;
    }

    uint64_t try_all_input_permutations(std::vector<uint64_t>& digits, uint32_t index) {
        // if index == 9, try this permutation
        if (index == 9)
            return try_input_permutation(digits);
        // otherwise, we want to try swapping the 'index' element with each right element
        // including itself (no swap)
        uint64_t max = 0;
        for (uint32_t swap_index = index; swap_index <= 9; swap_index++) {
            std::vector<uint64_t> digits_copy = digits;
            swap_elems(digits_copy, index, swap_index);
            uint64_t sub_max = try_all_input_permutations(digits_copy, index+1);
            if (sub_max > max)
                max = sub_max;
        }
        return max;
    }

    uint64_t try_all_input_permutations() {
        std::vector<uint64_t> digits_original {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        uint64_t max = 0;
        for (uint32_t swap_index = 1; swap_index <= 9; swap_index++) {
            std::vector<uint64_t> digits_copy = digits_original;
            swap_elems(digits_copy, 0, swap_index);
            uint64_t sub_max = try_all_input_permutations(digits_copy, 1);
            if (sub_max > max)
                max = sub_max;
        }
        return max;
    }

    euler_result run() {
        return try_all_input_permutations();
    }

    euler_entry entry(run);
}
