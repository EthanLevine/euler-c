#include <fstream>
#include <vector>

#include "runner.hpp"
#include "problem067.hpp"

namespace problem067 {
    const uint32_t triangle_size = 100;

    std::vector<std::vector<uint32_t>> parse_resource() {
        std::ifstream file_stream("resource/problem067_triangle.txt");
        std::vector<std::vector<uint32_t>> triangle;

        for (uint32_t line_num = 1; line_num <= triangle_size; line_num++) {
            triangle.push_back(std::vector<uint32_t>());
            for (uint32_t x = 0; x < line_num; x++) {
                // read a number
                uint32_t num;
                file_stream >> num;
                triangle[line_num-1].push_back(num);
            }
        }

        return triangle;
    }

    void calc_path(std::vector<std::vector<uint32_t>>& triangle,
            std::vector<std::vector<uint32_t>>& path_totals,
            uint32_t row, uint32_t col) {
        if (path_totals[row][col] > 0) {
            return;
        } else if (row == triangle_size-1) {
            path_totals[row][col] = triangle[row][col];
        } else {
            calc_path(triangle, path_totals, row+1, col);
            calc_path(triangle, path_totals, row+1, col+1);
            uint32_t left_cost = path_totals[row+1][col];
            uint32_t right_cost = path_totals[row+1][col+1];
            path_totals[row][col] = triangle[row][col] + (left_cost > right_cost ? left_cost : right_cost);
        }

    }

    euler_result run() {
        std::vector<std::vector<uint32_t>> triangle = parse_resource();
        std::vector<std::vector<uint32_t>> path_totals;
        for (uint32_t x = 0; x < triangle_size; x++) {
            path_totals.push_back(std::vector<uint32_t>(x+1));
        }

        calc_path(triangle, path_totals, 0, 0);
        return path_totals[0][0];
    }

    euler_entry entry(run);
}
