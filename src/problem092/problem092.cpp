#include "runner.hpp"
#include "problem092.hpp"

namespace problem092 {
    uint64_t next_num(uint64_t num) {
        uint64_t next = 0;
        while (num > 0) {
            uint64_t digit = num % 10;
            next += digit*digit;
            num /= 10;
        }
        return next;
    }
    euler_result run() {
        uint64_t count89 = 0;
        const uint64_t limit = 10000000;
        for (uint64_t x = 1; x <= limit; x++) {
            uint64_t y = x;
            while (y != 1 && y != 89) {
                y = next_num(y);
            }
            if (y == 89)
                count89++;
        }
        return count89;
    }

    euler_entry entry(run);
}
