#include <fstream>
#include <set>
#include <utility>
#include <vector>

#include "runner.hpp"
#include "problem083.hpp"

namespace problem083 {
    const uint32_t matrix_size = 80;
    
    uint32_t get(const std::vector<uint32_t>& matrix, uint32_t row, uint32_t col) {
        return matrix[row*matrix_size + col];
    }

    void set(std::vector<uint32_t>& matrix, uint32_t row, uint32_t col, uint32_t val) {
        matrix[row*matrix_size + col] = val;
    }

    uint32_t get(const std::vector<uint32_t>& matrix, const std::pair<uint32_t, uint32_t>& pos) {
        return matrix[pos.first*matrix_size + pos.second];
    }

    void set(std::vector<uint32_t>& matrix, const std::pair<uint32_t, uint32_t>& pos, uint32_t val) {
        matrix[pos.first*matrix_size + pos.second] = val;
    }

    std::vector<std::pair<uint32_t, uint32_t>> get_neighbors(uint32_t row, uint32_t col) {
        std::vector<std::pair<uint32_t, uint32_t>> neighbors;
        if (row > 0)
            neighbors.push_back({row-1, col});
        if (row < matrix_size-1)
            neighbors.push_back({row+1, col});
        if (col > 0)
            neighbors.push_back({row, col-1});
        if (col < matrix_size-1)
            neighbors.push_back({row, col+1});
        return neighbors;
    }

    std::vector<std::pair<uint32_t, uint32_t>> get_neighbors(const std::pair<uint32_t, uint32_t>& pos) {
        return get_neighbors(pos.first, pos.second);
    }

    std::vector<uint32_t> parse_resource() {
        std::ifstream file_stream("resource/problem081_082_083_matrix.txt");
        std::vector<uint32_t> matrix(matrix_size*matrix_size);
        for (uint32_t row = 0; row < matrix_size; row++) {
            for (uint32_t col = 0; col < matrix_size; col++) {
                uint32_t val;
                file_stream >> val;
                if (col != matrix_size-1) {
                    char comma;
                    file_stream >> comma;
                }
                set(matrix, row, col, val);
            }
        }
        return matrix;
    }

    uint32_t get_min_path(const std::vector<uint32_t>& matrix) {
        // we want the smallest path from (0,0) to (matrix_size-1,matrix_size-1)
        const uint32_t max_cost = 1 << 30;
        std::vector<uint32_t> costs(matrix_size*matrix_size, max_cost);
        std::set<std::pair<uint32_t, uint32_t>> neighbors {{0, 1}, {1, 0}};
        set(costs, 0, 0, get(matrix, 0, 0));
        while (get(costs, matrix_size-1, matrix_size-1) == max_cost) {
            // find the lowest-cost neighbor
            std::pair<uint32_t, uint32_t> min_neighbor;
            uint32_t min_neighbor_cost = max_cost;
            for (const std::pair<uint32_t, uint32_t>& neighbor : neighbors) {
                uint32_t neighbor_cost = max_cost;
                for (const std::pair<uint32_t, uint32_t>& adj : get_neighbors(neighbor)) {
                    uint32_t adj_cost = get(costs, adj);
                    if (adj_cost < neighbor_cost)
                        neighbor_cost = adj_cost;
                }
                neighbor_cost += get(matrix, neighbor);
                if (min_neighbor_cost > neighbor_cost) {
                    min_neighbor_cost = neighbor_cost;
                    min_neighbor = neighbor;
                }
            }
            // expand on 'min_neighbor'
            set(costs, min_neighbor, min_neighbor_cost);
            neighbors.erase(min_neighbor);
            for (const std::pair<uint32_t, uint32_t>& new_neighbor : get_neighbors(min_neighbor)) {
                if (get(costs, new_neighbor) == max_cost) {
                    neighbors.insert(new_neighbor);
                }
            }
        }
        return get(costs, matrix_size-1, matrix_size-1);
    }

    euler_result run() {
        const std::vector<uint32_t> matrix = parse_resource();
        return get_min_path(matrix);
    }

    euler_entry entry(run);
}
