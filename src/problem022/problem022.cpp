#include <algorithm>
#include <fstream>
#include <string>
#include <vector>

#include "runner.hpp"
#include "problem022.hpp"

namespace problem022 {
    std::vector<std::string> parse_resource() {
        std::ifstream file_stream("resource/problem022_names.txt");
        std::vector<std::string> names;

        char name_buffer[20];
        while (file_stream.getline(name_buffer, 20, ',')) {
            std::string quoted_name(name_buffer);
            names.push_back(quoted_name.substr(1, quoted_name.size()-2));
        }

        return names;
    }

    euler_result run() {
        // get the names
        std::vector<std::string> names = parse_resource();
        // sort the names
        std::sort(names.begin(), names.end());
        // score the names
        uint64_t total = 0;
        uint64_t multiplier = 1;
        for (std::string& name : names) {
            uint64_t name_total = 0;
            for (char letter : name) {
                name_total += letter - 'A' + 1;
            }
            total += name_total * multiplier;
            multiplier++;
        }
        return total;
    }

    euler_entry entry(run);
}
