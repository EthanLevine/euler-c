#include <gmpxx.h>

#include "runner.hpp"
#include "problem053.hpp"

namespace problem053 {
    const uint32_t limit = 1000000;
    // returns 'true' if nCr > 1000000
    bool is_combo_large(uint32_t n, uint32_t r) {
        mpz_class n_fac, r_fac, nr_fac;
        mpz_fac_ui(n_fac.get_mpz_t(), n);
        mpz_fac_ui(r_fac.get_mpz_t(), r);
        mpz_fac_ui(nr_fac.get_mpz_t(), n-r);
        mpz_class result = n_fac / (r_fac * nr_fac);
        return result > limit;
    }

    euler_result run() {
        // a rather brute force approach
        uint32_t count = 0;
        for (uint32_t n = 1; n <= 100; n++) {
            for (uint32_t r = 1; r < n; r++) {
                if (is_combo_large(n, r))
                    count++;
            }
        }
        return count;
    }

    euler_entry entry(run);
}
