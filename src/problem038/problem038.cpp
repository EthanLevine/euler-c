#include <algorithm>
#include <cmath>
#include <vector>

#include "runner.hpp"
#include "problem038.hpp"

namespace problem038 {
    bool include_digits(std::vector<bool>& digits, uint32_t num) {
        while (num > 0) {
            uint32_t digit = num % 10;
            if (digit == 0)
                return false;
            if (digits[digit])
                return false;
            else
                digits[digit] = true;
            num /= 10;
        }
        return true;
    }

    uint32_t concat(uint32_t a, uint32_t b) {
        // find #digits in b
        uint32_t b_mask = (uint32_t)pow(10,floor(log10(b))+1);
        return a*b_mask + b;
    }

    uint32_t concat(std::vector<uint32_t>& elems) {
        if (elems.size() == 1)
            return elems[0];
        uint32_t tail = elems.back();
        elems.erase(elems.end() - 1);
        return concat(concat(elems), tail);
    }

    // tries to get a panditigal, but returns 0 otherwise
    uint32_t get_pandigital(uint32_t num) {
        std::vector<bool> digits(10);
        std::vector<uint32_t> products;
        for (uint32_t x = 1; ; x++) {
            uint32_t product = num*x;
            products.push_back(product);
            if (!include_digits(digits, product))
                return 0;
            if (std::all_of(digits.begin() + 1, digits.end(), [](bool b){return b;}))
                break;
        }
        // we found a pandigital... now to get the largest combination
        return concat(products);
    }

    euler_result run() {
        uint32_t largest_pandigital = 0;
        for (uint32_t x = 2; x <= 9999; x++) {
            uint32_t pandigital = get_pandigital(x);
            if (pandigital > largest_pandigital)
                largest_pandigital = pandigital;
        }
        return largest_pandigital;
    }

    euler_entry entry(run);
}
