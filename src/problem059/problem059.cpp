#include <fstream>
#include <string>

#include "runner.hpp"
#include "problem059.hpp"

namespace problem059 {
    // letter frequencies in the english language
    // data from Wikipedia
    const double letter_freqs[] = {
        8.167,
        1.492,
        2.782,
        4.253,
        12.702,
        2.228,
        2.015,
        6.094,
        6.966,
        0.153,
        0.772,
        4.025,
        2.406,
        6.749,
        7.507,
        1.929,
        0.095,
        5.987,
        6.327,
        9.056,
        2.758,
        0.978,
        2.360,
        0.150,
        1.974,
        0.074,
    };

    std::string parse_resource() {
        std::ifstream file_stream("resource/problem059_cipher1.txt");
        std::string contents;
        std::string ascii_code;
        while (std::getline(file_stream, ascii_code, ',')) {
            contents.push_back((char)std::stoul(ascii_code));
        }
        return contents;
    }

    std::string decrypt(const std::string& ciphertext, const std::string& password) {
        std::string plaintext = ciphertext;
        uint32_t pass_pos = 0;
        for (char& c : plaintext) {
            c ^= password[pass_pos];
            pass_pos++;
            pass_pos %= 3;
        }
        return plaintext;
    }

    double score_plaintext(const std::string& plaintext) {
        double score = 0;
        for (char c : plaintext) {
            if (c >= 'a' && c <= 'z')
                score += letter_freqs[c - 'a'];
            else if (c >= 'A' && c <= 'Z')
                score += letter_freqs[c - 'A'];
        }
        return score;
    }

    uint32_t total_plaintext(const std::string& plaintext) {
        uint32_t total = 0;
        for (char c : plaintext)
            total += c;
        return total;
    }

    euler_result run() {
        const std::string ciphertext = parse_resource();
        double best_score = 0;
        std::string best_password;
        for (char first = 'a'; first <= 'z'; first++) {
            for (char second = 'a'; second <= 'z'; second++) {
                for (char third = 'a'; third <= 'z'; third++) {
                    const std::string password {first, second, third};
                    const std::string plaintext = decrypt(ciphertext, password);
                    double score = score_plaintext(plaintext);
                    if (score > best_score) {
                        best_score = score;
                        best_password = password;
                    }
                }
            }
        }
        return total_plaintext(decrypt(ciphertext, best_password));
    }

    euler_entry entry(run);
}
