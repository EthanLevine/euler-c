#include <fstream>
#include <utility>
#include <vector>

#include "runner.hpp"
#include "problem081.hpp"

namespace problem081 {
    const uint32_t matrix_size = 80;
    
    uint32_t get(const std::vector<uint32_t>& matrix, uint32_t row, uint32_t col) {
        return matrix[row*matrix_size + col];
    }

    void set(std::vector<uint32_t>& matrix, uint32_t row, uint32_t col, uint32_t val) {
        matrix[row*matrix_size + col] = val;
    }

    uint32_t get(const std::vector<uint32_t>& matrix, const std::pair<uint32_t, uint32_t>& pos) {
        return matrix[pos.first*matrix_size + pos.second];
    }

    void set(std::vector<uint32_t>& matrix, const std::pair<uint32_t, uint32_t>& pos, uint32_t val) {
        matrix[pos.first*matrix_size + pos.second] = val;
    }

    std::vector<uint32_t> parse_resource() {
        std::ifstream file_stream("resource/problem081_082_083_matrix.txt");
        std::vector<uint32_t> matrix(matrix_size*matrix_size);
        for (uint32_t row = 0; row < matrix_size; row++) {
            for (uint32_t col = 0; col < matrix_size; col++) {
                uint32_t val;
                file_stream >> val;
                if (col != matrix_size-1) {
                    char comma;
                    file_stream >> comma;
                }
                set(matrix, row, col, val);
            }
        }
        return matrix;
    }

    uint32_t find_min_cost(std::vector<uint32_t>& matrix) {
        std::vector<uint32_t> min_costs(matrix_size * matrix_size);
        // Set the rightmost and bottom edges.
        set(min_costs, matrix_size-1, matrix_size-1, get(matrix, matrix_size-1, matrix_size-1));
        for (uint32_t row = matrix_size-2; ; row--) {
            uint32_t cost = get(min_costs, row+1, matrix_size-1) + get(matrix, row, matrix_size-1);
            set(min_costs, row, matrix_size-1, cost);
            if (row == 0)
                break;
        }
        for (uint32_t col = matrix_size-2; ; col--) {
            uint32_t cost = get(min_costs, matrix_size-1, col+1) + get(matrix, matrix_size-1, col);
            set(min_costs, matrix_size-1, col, cost);
            if (col == 0)
                break;
        }
        // Iterate backwards
        for (uint32_t row = matrix_size-2; ; row--) {
            for (uint32_t col = matrix_size-2; ; col--) {
                uint32_t self_cost = get(matrix, row, col);
                uint32_t cost_right = self_cost + get(min_costs, row, col+1);
                uint32_t cost_down = self_cost + get(min_costs, row+1, col);
                if (cost_right < cost_down)
                    set(min_costs, row, col, cost_right);
                else
                    set(min_costs, row, col, cost_down);
                if (col == 0)
                    break;
            }
            if (row == 0)
                break;
        }
        return get(min_costs, 0, 0);
    }

    euler_result run() {
        std::vector<uint32_t> matrix = parse_resource();
        return find_min_cost(matrix);
    }

    euler_entry entry(run);
}
