#include <algorithm>
#include <gmpxx.h>
#include <vector>

#include "runner.hpp"
#include "problem055.hpp"

namespace problem055 {
    bool is_lychrel(uint32_t num) {
        mpz_class num_mpz(num);
        std::string num_str = num_mpz.get_str();
        std::string num_rstr = num_str;
        std::reverse(num_rstr.begin(), num_rstr.end());
        mpz_class num_rmpz(num_rstr, 10);
        num_mpz += num_rmpz;
        for (uint32_t it_count = 1; it_count < 50; it_count++) {
            // see if num_mpz is a palindrome (one iteration already executed)
            num_str = num_mpz.get_str();
            num_rstr = num_str;
            std::reverse(num_rstr.begin(), num_rstr.end());
            if (num_str == num_rstr)
                // palindrome found - this is NOT a lychrel number
                return false;
            // set num_mpz up for the next iteration
            num_rmpz = mpz_class(num_rstr, 10);
            num_mpz += num_rmpz;
        }
        // ran out of iterations - this is a lychrel number
        return true;
    }

    euler_result run() {
        uint32_t count = 0;
        for (uint32_t x = 1; x < 10000; x++) {
            if (is_lychrel(x))
                count++;
        }
        return count;
    }

    euler_entry entry(run);
}
