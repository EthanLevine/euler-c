#include "runner.hpp"
#include "problem173.hpp"

namespace problem173 {
    uint32_t count_combinations(uint64_t total) {
        if (total % 4 != 0)
            return 0;
        const uint64_t quarter_total = total/4;
        uint32_t count = 0;
        // try odd rings
        uint64_t low = 2;
        uint64_t high = 0;
        uint64_t sum = 0;
        do {
            if (sum == quarter_total)
                count++;
            if (sum < quarter_total) {
                high += 2;
                sum += high;
            } else {
                sum -= low;
                low += 2;
            }
        } while (low <= high);
        // try even rings
        low = 3;
        high = 1;
        sum = 0;
        do {
            if (sum == quarter_total)
                count++;
            if (sum < quarter_total) {
                high += 2;
                sum += high;
            } else {
                sum -= low;
                low += 2;
            }
        } while (low <= high);
        return count;
    }

    euler_result run() {
        const uint64_t total = 1000000;
        uint32_t count = 0;
        for (uint64_t x = 8; x <= total; x += 4) {
            count += count_combinations(x);
        }
        return count;
    }

    euler_entry entry(run);
}
