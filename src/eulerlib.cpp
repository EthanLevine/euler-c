#include <cmath>
#include <cstdint>

#include "eulerlib.hpp"

// Triangular numbers
uint32_t tri_num(uint32_t index) {
    return (index*(index+1))/2;
}
uint64_t tri_num(uint64_t index) {
    return (index*(index+1))/2;
}
bool is_tri_num(uint32_t num) {
    uint32_t disc = 1+8*num;
    uint32_t root = (uint32_t)sqrt(disc);
    return (root*root == disc) && (root % 2 == 1);
}
bool is_tri_num(uint64_t num) {
    uint64_t disc = 1+8*num;
    uint64_t root = (uint64_t)sqrt(disc);
    return (root*root == disc) && (root % 2 == 1);
}

// Pentagonal numbers
uint32_t pent_num(uint32_t index) {
    return (index*(3*index-1))/2;
}
uint64_t pent_num(uint64_t index) {
    return (index*(3*index-1))/2;
}
bool is_pent_num(uint32_t num) {
    uint32_t disc = 1+24*num;
    uint32_t root = (uint32_t)sqrt(disc);
    return (root*root == disc) && ((1-root) % 6 == 0 || (1+root) % 6 == 0);
}
bool is_pent_num(uint64_t num) {
    uint64_t disc = 1+24*num;
    uint64_t root = (uint64_t)sqrt(disc);
    return (root*root == disc) && ((1-root) % 6 == 0 || (1+root) % 6 == 0);
}

// Hexagonal numbers
uint32_t hex_num(uint32_t index) {
    return index*(2*index-1);
}
uint64_t hex_num(uint64_t index) {
    return index*(2*index-1);
}
bool is_hex_num(uint32_t num) {
    uint32_t disc = 1+8*num;
    uint32_t root = (uint32_t)sqrt(disc);
    return (root*root == disc) && ((1-root) % 4 == 0 || (1+root) % 4 == 0);
}
bool is_hex_num(uint64_t num) {
    uint64_t disc = 1+8*num;
    uint64_t root = (uint64_t)sqrt(disc);
    return (root*root == disc) && ((1-root) % 4 == 0 || (1+root) % 4 == 0);
}

uint32_t gcd(uint32_t a, uint32_t b) {
    while (a != 0) {
        uint32_t temp = a;
        a = b % temp;
        b = temp;
    }
    return b;
}
uint64_t gcd(uint64_t a, uint64_t b) {
    while (a != 0) {
        uint64_t temp = a;
        a = b % temp;
        b = temp;
    }
    return b;
}