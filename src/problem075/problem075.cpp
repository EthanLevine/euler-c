#include <vector>

#include "eulerlib.hpp"
#include "runner.hpp"
#include "problem075.hpp"

namespace problem075 {
    void set_singular_perimeters(std::vector<uint8_t>& perims) {
        uint64_t limit = perims.size() - 1;
        for (uint64_t n = 1; ; n++) {
            if (n*(4*n+6)+2 > limit)
                break;
            for (uint64_t m = n+1; ; m += 2) {
                if (gcd(n, m) != 1)
                    continue;
                uint64_t primitive_perim = 2*m*(m+n);
                if (primitive_perim > limit)
                    break;
                for (uint64_t x = primitive_perim; x <= limit; x += primitive_perim) {
                    if (perims[x] < 2)
                        perims[x]++;
                }
            }
        }
    }

    euler_result run() {
        const uint64_t limit = 1500000;

        std::vector<uint8_t> vec(limit+1);
        set_singular_perimeters(vec);
        uint32_t count = 0;
        for (uint64_t x = 12; x <= limit; x += 2) {
            if (vec[x] == 1)
                count++;
        }
        return count;
    }

    euler_entry entry(run);
}
