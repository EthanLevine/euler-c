#include <vector>

#include "runner.hpp"
#include "problem034.hpp"

namespace problem034 {
    uint64_t fac(uint64_t n) {
        uint64_t result = n;
        n--;
        while (n > 1) {
            result *= n;
            n--;
        }
        return result;
    }

    uint64_t digit_fac_sum(uint64_t num, const std::vector<uint64_t>& factorials) {
        uint64_t sum = 0;
        while (num > 0) {
            sum += factorials[num % 10];
            num /= 10;
        }
        return sum;
    }
    
    euler_result run() {
        // this limit is probably higher than it needs to be...
        const uint64_t limit = 3000000;
        uint64_t sum = 0;
        std::vector<uint64_t> factorials(10);
        factorials[0] = 1;
        for (uint64_t x = 1; x <= 9; x++) {
            factorials[x] = fac(x);
        }
        for (uint64_t x = 10; x < limit; x++) {
            if (x == digit_fac_sum(x, factorials)) {
                sum += x;
            }
        }
        return sum;
    }

    euler_entry entry(run);
}
