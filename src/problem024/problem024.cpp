#include <vector>

#include "runner.hpp"
#include "problem024.hpp"

namespace problem024 {
    uint32_t fac(uint32_t n) {
        uint32_t result = 1;
        while (n > 1) {
            result *= n;
            n--;
        }
        return result;
    }

    void get_permutation(std::vector<uint32_t>& elements, uint64_t n) {
        // if n == 0, no need to permute anything
        if (n == 0)
            return;
        // get the number of "children" permutations
        uint32_t size = elements.size();
        uint32_t child_count = fac(size-1);
        // extract and remove the new head
        uint32_t head_index = n / child_count;
        uint32_t head = elements[head_index];
        elements.erase(elements.begin() + head_index);
        // perform the rest of the permutation
        get_permutation(elements, n % child_count);
        // insert the new head back
        elements.insert(elements.begin(), head);
    }

    euler_result run() {
        std::vector<uint32_t> elements{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        get_permutation(elements, 999999);
        uint64_t value = 0;
        for (uint32_t element : elements) {
            value *= 10;
            value += element;
        }
        return value;
    }

    euler_entry entry(run);
}
