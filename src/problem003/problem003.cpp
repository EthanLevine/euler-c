#include <cmath>

#include "runner.hpp"
#include "problem003.hpp"

namespace problem003 {
    uint64_t largest_prime_factor(uint64_t num) {
        // See if any numbers from 2 to floor(sqrt(num)) are factors.
        uint64_t max_factor = (uint64_t)floor(sqrt((double)num));
        for (uint64_t factor = 2; factor <= max_factor; factor++) {
            // see if 'factor' is actually a factor
            if ((num % factor) == 0) {
                // If so, return the largest prime factor of its factor pair.
                return largest_prime_factor(num / factor);
            }
        }
        // If no factors were found, then 'num' must be prime, so just return it.
        return num;
    }

    euler_result run() {
        uint64_t num = 600851475143;
        return largest_prime_factor(num);
    }

    euler_entry entry(run);
}