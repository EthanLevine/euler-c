#include <gmpxx.h>
#include <string>

#include "runner.hpp"
#include "problem025.hpp"

namespace problem025 {
    euler_result run() {
        mpz_class a(0);
        mpz_class b(1);
        mpz_class temp;
        uint64_t term = 1;
        while (b.get_str().size() < 1000) {
            temp = b;
            b = a + b;
            a = temp;
            term++;
        }
        return term;
    }

    euler_entry entry(run);
}
